<?php session_start();

  //require_once("../SessionHandler.php");
  require_once("../source/User.class.php");
  require_once("../source/CurrentUser.class.php");
  
  class CurrentUserClassTest extends PHPUnit_Framework_TestCase{
   
    public function setUp(){
      
      $userObj = new User();
      
       $email = 'ab@cd.com';
      $password = '123';
      
      $userObj->setEmail($email, false);
      $userObj->setPassword($password, false);
      
      $userObj->login();
	
      
    }
    public function tearDown(){}
    
    public function testLogout(){
      $cuObj = new CurrentUser();
      
      //$email = 'ab@cd.com';
      //$password = '123';
      
      //$cuObj->setEmail($email, false);
      //$cuObj->setPassword($password, false);
      $cuObj->logout();
      //$this->assertTrue($cuObj->logout() !== false);
      $this->assertEquals(null, $cuObj->getCurrentUserId());
    }
  
  }


?>