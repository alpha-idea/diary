SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `diary` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
CREATE SCHEMA IF NOT EXISTS `diary` DEFAULT CHARACTER SET utf8 ;
USE `diary` ;

-- -----------------------------------------------------
-- Table `diary`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(255),
  `firstname` VARCHAR(45) NULL,
  `middlename` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `gender` ENUM('m','f') NULL DEFAULT 'm',
  `birthday` VARCHAR(45) NULL,
  `profilepic` VARCHAR(255) NULL,
  `datecreated` DATETIME NULL,
  `lastlogin` DATETIME NULL,
  `lastactive` DATETIME NULL,
  `mobile_phone` VARCHAR(45) NULL,
  `business_phone` VARCHAR(45) NULL,
  `home_phone` VARCHAR(45) NULL,
  `fax_number` VARCHAR(45) NULL,
  `driving_licence` VARCHAR(45) NULL,
  `passport_num` VARCHAR(45) NULL,
  `bloodgrp` VARCHAR(5) NULL,
  `likes` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`entries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`entries` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `entry` TEXT NULL,
  `entryvisibility` ENUM('private','public') NOT NULL DEFAULT 'private',
  `entrydate` DATE NOT NULL,
  `usertime` TIME UNIQUE NOT NULL,
  `entrytime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userid` INT NOT NULL,
  `likes` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  FULLTEXT(entrydate,entry),
  CONSTRAINT `fk_entries_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = MyISAM;


-- -----------------------------------------------------
-- Table `diary`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `tasktitle` TEXT NULL,
  `taskdesc` TEXT NULL,
  `taskdate` VARCHAR(45) NULL,
  `tasktime` TIME,
  `userid` INT NOT NULL,
  `alarmid` INT NULL,
  PRIMARY KEY (`id`, `userid`),
  INDEX `fk_tasks_User1_idx` (`userid` ASC),
  CONSTRAINT `fk_tasks_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`alarm_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm_types` (
  `id` INT NOT NULL,
  `alarm_type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'alarm types: birthdays, anniversries, memo, reminders, tasks';


-- -----------------------------------------------------
-- Table `diary`.`alarm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_time_created` DATETIME NULL,
  `alarm_date` DATE NULL,
  `alarm_time` TIME NULL,
  `userid` INT NOT NULL,
  `alarm_types_id` INT NOT NULL,
  PRIMARY KEY (`id`, `userid`, `alarm_types_id`),
  INDEX `fk_alarm_User1_idx` (`userid` ASC),
  INDEX `fk_alarm_alarm_types1_idx` (`alarm_types_id` ASC),
  CONSTRAINT `fk_alarm_User`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alarm_types`
    FOREIGN KEY (`alarm_types_id`)
    REFERENCES `diary`.`alarm_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`contacts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `prefix` varchar(15) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `homePhone` varchar(45) DEFAULT NULL,
  `workPhone` varchar(45) DEFAULT NULL,
  `mobilePhone` varchar(45) DEFAULT NULL,
  `prefname` varchar(55) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `bday` varchar(10) NOT NULL,
  `adrnum` varchar(20) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `county` varchar(255) NOT NULL,
  `pocode` varchar(20) NOT NULL,
  `country` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `fb` varchar(255) NOT NULL,
  `tw` varchar(255) NOT NULL,
  `gp` varchar(255) NOT NULL,
  `yt` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`calender_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`calender_type` (
  `userid` INT NOT NULL,
  PRIMARY KEY (`userid`),
  CONSTRAINT `fk_calender_type_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`anniversary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`anniversary` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `contacts_id` INT NOT NULL,
  PRIMARY KEY (`id`, `contacts_id`),
  INDEX `fk_anniversary_contacts1_idx` (`contacts_id` ASC),
  CONSTRAINT `fk_anniversary_contacts1`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `diary`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`birthday`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`birthday` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `contacts_id` INT NOT NULL,
  PRIMARY KEY (`id`, `contacts_id`),
  INDEX `fk_birthday_contacts1_idx` (`contacts_id` ASC),
  CONSTRAINT `fk_birthday_contacts`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `diary`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `diary`.`friendship`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`friendship` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `friendid` INT NOT NULL,
  UNIQUE(`userid`,`friendid`),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_User`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_friend`
    FOREIGN KEY (`friendid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`following`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`following` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `followerid` INT NOT NULL,
  UNIQUE(`userid`,`followerid`),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_user_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_follower`
    FOREIGN KEY (`followerid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`email` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) UNIQUE NOT NULL,
  `verify_code` VARCHAR(255) NULL,
  `verified` TINYINT(1) DEFAULT 0,
  `userid` INT NOT NULL,
   PRIMARY KEY (`id`),
   FOREIGN KEY (`userid`) REFERENCES user(`id`)
    )
ENGINE = InnoDB;

USE `diary` ;



-- -----------------------------------------------------
-- Table `diary`.`entrylikes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`entrylikes` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `entryid` INT NOT NULL,
  `userid` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE (`entryid`,`userid`),
  FOREIGN KEY (`userid`) REFERENCES `diary`.`user` (`id`),
  FOREIGN KEY (`entryid`) REFERENCES `diary`.`entries` (`id`)
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `diary`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `task` TEXT NULL DEFAULT NULL,
  `taskdate` VARCHAR(45) NULL DEFAULT NULL,
  `tasktime` DATE NULL,
  `userid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tasks_User1_idx` (`userid`),
  CONSTRAINT `fk_tasks_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`alarm_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `alarm_type` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
COMMENT = 'alarm types: birthdays, anniversries, memo, reminders, tasks';


-- -----------------------------------------------------
-- Table `diary`.`alarm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `typesid` INT NOT NULL,
  `date_time_created` DATETIME NULL DEFAULT NULL,
  `alarm_date` DATE NULL DEFAULT NULL,
  `alarm_time` TIME NULL DEFAULT NULL,
  `userid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_alarm_alarm_types1_idx` (`typesid`),
  INDEX `fk_alarm_User1_idx` (`userid`),
  CONSTRAINT `fk_alarm_alarm_types1`
    FOREIGN KEY (`typesid`)
    REFERENCES `diary`.`alarm_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alarm_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `diary`.`calender_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`calender_type` (
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`User_userid`),
  CONSTRAINT `fk_calender_type_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`anniversary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`anniversary` (
  `anniversary_id` INT NOT NULL,
  `contacts_idcontacts` INT NOT NULL,
  PRIMARY KEY (`anniversary_id`, `contacts_idcontacts`),
  INDEX `fk_anniversary_contacts1_idx` (`contacts_idcontacts` ASC),
  CONSTRAINT `fk_anniversary_contacts1`
    FOREIGN KEY (`contacts_idcontacts`)
    REFERENCES `diary`.`contacts` (`idcontacts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`birthday`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`birthday` (
  `birthday_id` INT NOT NULL AUTO_INCREMENT,
  `contacts_idcontacts` INT NOT NULL,
  PRIMARY KEY (`birthday_id`, `contacts_idcontacts`),
  INDEX `fk_birthday_contacts1_idx` (`contacts_idcontacts` ASC),
  CONSTRAINT `fk_birthday_contacts1`
    FOREIGN KEY (`contacts_idcontacts`)
    REFERENCES `diary`.`contacts` (`idcontacts`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`user_contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`user_contacts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `contactsid` INT NOT NULL,
  PRIMARY KEY (`id`, `userid`, `contactsid`),
  INDEX `fk_user_contacts_User1_idx` (`userid`),
  INDEX `fk_user_contacts_contacts1_idx` (`contactsid` ASC),
  CONSTRAINT `fk_user_contacts_User1`
    FOREIGN KEY (`userid`)
    REFERENCES `diary`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contacts_contacts1`
    FOREIGN KEY (`contactsid`)
    REFERENCES `diary`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `diary`.`cookies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`cookies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `cookie` varchar(255) UNIQUE NOT NULL,
  PRIMARY KEY (`id`),  
   FOREIGN KEY (`userid`)  REFERENCES `diary`.`user` (`id`)
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `diary`.`sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`sessions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `session_id` varchar(255) UNIQUE NOT NULL,
  PRIMARY KEY (`id`),  
   FOREIGN KEY (`userid`)  REFERENCES `diary`.`user` (`id`)
  )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `diary`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `entryid` INT NOT NULL,
  `commentdate` DATE NOT NULL,
  `usertime` TIME UNIQUE NOT NULL,
  `commenttime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` TEXT NULL,
  PRIMARY KEY (`id`),  
   FOREIGN KEY (`userid`)  REFERENCES `diary`.`user` (`id`),
   FOREIGN KEY (`entryid`)  REFERENCES `diary`.`entries` (`id`)
  )
ENGINE = InnoDB;

USE `diary` ;



USE `diary` ;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
