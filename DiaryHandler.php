<?php
include_once('SessionHandler.php');
include_once('source/Diary.class.php');
include_once('source/Comment.class.php');


if(isset($_POST['diary_edit']) && $_POST['diary_edit']==1 && isset($_POST['Entry_id']) && $_POST['Entry_id']>0){
	
		EntryEdit($_POST['Entry_id']);

	
	}

if(isset($_POST['diary_delete']) && $_POST['diary_delete']=='1' && isset($_POST['Entry_id']) && $_POST['Entry_id']>0){
	
		EntryDelete($_POST['Entry_id']);

	
	}
	
if(isset($_POST['diary_auto_save']) && $_POST['diary_auto_save']=='1' ){
	
			EntryAutoSave();

	
	}
	
if(isset($_POST['diary_add']) && $_POST['diary_add']=='1' && isset($_POST['EntryText']) && isset($_POST['EntryDate'])&& isset($_POST['UserTime']) ){
	
	
			EntryAdd();
	}

if(isset($_POST['diary_get_by_date']) && $_POST['diary_get_by_date']=='1' && isset($_POST['entry_date']) ){
	
		getEntriesForTheDay($_POST['entry_date']);
	
	}
	
if(isset($_GET['diary_like_entry']) && isset($_GET['Entry_id'])){
		
		setLikeForEntry($_GET['Entry_id']);
	
	}
	
	
if(isset($_POST['comment_add']) && $_POST['comment_add']=='1' && isset($_POST['CommentText']) && isset($_POST['CommentDate'])&& isset($_POST['UserTime']) ){
	
	
			CommentAdd();
	}
/**************************************FUNCTIONS*********************************************************/

function EntryEdit($id){
	
		$Entry = new Entry($id);
		//CODE REMAINING
	
	
	}
	
function EntryDelete($id){
	
		$Entry = new Entry($id);
		if($Entry->deleteEntry()){
				
				echo json_encode(1);
			
			}
	}
	
function EntryAdd(){
	error_log("called EntryAdd()");
	//if(preg_match("/^\d{4}-\d{2}-\d{2}$/", $date)){
		$Entry  = new Diary();
		//$Entry->setEntryDate($date, false);
		//$Entry->setUserTime($time, false);
		$Entry->addNewEntry();
	//}
}


function getEntriesForTheMonth($yyyy_mm){
		
		$Entry = new Diary();
		
		$Entry->getEntriesForTheMonth($yyyy_mm);
	
	}
	
function getEntriesForTheDay($day){
		$Entry = new Diary();
		echo $Entry->getEntriesForTheDay($day);
	
}

function EntryAutoSave(){
		$Entry = new Diary();
		$Entry->autoSave();
	
	}
	
function EntrySave(){
		
		$Entry = new Diary();
		//$Entry->SaveEntry();
	
	}
	
function setLikeForEntry($EntryId){
		
		$Entry = new Diary($EntryId);
		if($Entry->setLiked()){
				
				$Entry->setLike();
				
				echo json_encode($Entry->getLikes());
			}
	
	}
	
function CommentAdd(){
	
	error_log("called CommentAdd()");

		$Comment  = new Comment();

		$Comment->addNewComment();
	
}
?>
