<?php 
include_once("$_SERVER[DOCUMENT_ROOT]/diary/SessionHandler.php"); 
?>
<style>
.tasklist{
        overflow-y:scroll;
        max-height:200px;
}
#list_view{
	font-family: Sans-Serif;
	font-size: 14px;
	margin: -15px 45px;
	width: 400px;
	text-align: center;
	border-collapse: collapse;
}
#list_view th{
	font-size: 16px;
	font-weight: normal;
	padding: 10px 8px;
	color: #000;
	text-shadow: 0px 1px 0px #fff;
	background: #fcfcfc; 
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZmNmYyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iI2U1ZTVlNSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjZGNkY2QiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #fcfcfc 0%, #e5e5e5 50%, #cdcdcd 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(50%,#e5e5e5), color-stop(100%,#cdcdcd));
	background: -webkit-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	background: -o-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%); 
	background: -ms-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	background: linear-gradient(to bottom,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#cdcdcd',GradientType=0 );
	border: 1px solid #fff;
}
#list_view tr{
	color: #000;
	text-shadow: 0px 1px 0px #fff;
}
#list_view td{
	padding: 8px;
	border: 1px solid #fff;
}
#list_view .even{
	background: #DFDFDF; 
}
#list_view tr:hover{
	background: #ced6df;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2NlZDZkZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iI2MyY2VkYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNiNmM2ZDciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #ced6df 0%, #c2cedb 50%, #b6c6d7 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ced6df), color-stop(50%,#c2cedb), color-stop(100%,#b6c6d7));
	background: -webkit-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: -o-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: -ms-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: linear-gradient(to bottom,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ced6df', endColorstr='#b6c6d7',GradientType=0 );
}
.scroll_tbl{
  width:90%;
  margin:auto;
}

.scroll_tbl thead{
  background:#1ABC9C;
  color:white;
}

.scroll_tbl th, .scroll_tbl td{
  text-align:center;
  padding:5px 0;
}

.scroll_tbl tbody tr:nth-child(even){
  background:#ECF0F1;
}

.scroll_tbl tbody tr:hover{
background:#BDC3C7;
  color:#FFFFFF;
}

.fixed{
  top: 152px;
  position:fixed;
  width:auto;
  display:none;
  border:none;
}
.fixed th{
    padding: 10px 0px !important;
}
.scrollMore{
  margin-top:600px;
}

.up{
  cursor:pointer;
}

#add-task-status{
		display:none;
}
</style>

<span><?php echo $_REQUEST['clicked_date']; echo " [".$_REQUEST['en_day']."]";?></span>
<!-- TAB BAR FOR EACH DAY GOES FROM HERE-->				
<div id="tabs">
	<ul>
		
		<li><a href="#task">Tasks</a></li>
                                                     
	</ul>
	

 <div id="task">
<div id="task-wrapper"><h3>Tasks For the Day (<?php echo $_REQUEST['clicked_date'];?>)</h3>
<div class="list-task-box">
	
		
		<div class='tasklist'>
	<table id="list_view" border="0" align="center">
  <thead>
    <tr>
	  
      <th scope="col">Task</th>
      <th scope="col">Description </th>
      <th scope="col">Time </th>
      <!-- <th scope="col">Alarm</th> -->
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
 </thead>	
			
  <tbody>
	  <?php
		//include('../source/Tasks.class.php');
		
		$task = new Task();
		$taskArray = $task->getTasksForTheDay($_REQUEST['clicked_date']);
		error_log(print_r($taskArray,true));
		if(isset($taskArray) && !empty($taskArray)){
		foreach($taskArray as $ta){
			
	  ?>
    <tr>
	  <td><?php echo $ta->getTaskTitle(); ?></td>
      <td><?php echo $ta->getTaskDescription(); ?></td>
      <td><?php echo $ta->getTaskTime(); ?></td>
      <td><input type="button" class="edit_task" value="edit" data-id="<?php echo $ta->getTaskId(); ?>" onclick="<?php echo 'editTask('.$ta->getTaskId().');'; ?>" /></td>
      <td><input type="button" class="delete_task" value="delete" data-id="<?php echo $ta->getTaskId(); ?>" onclick="<?php echo 'deleteTask('.$ta->getTaskId().');'; ?>" /></td>
    </tr>
    <?php } 
	}
    ?>
 
  </tbody>
</table>
		
		</div>
	

</div>	 
	 <!-- end of list-task-box -->
	 <h3>Add New Task For <?php echo $_REQUEST['clicked_date'];?></h3>
 <div class="add-task-box">
  <form name="task-form" action='unimplemented'>
    <table>
        <tr>
         <td width="70">Title</td>
         <td width="170"><input type="text" name="TaskTitle" id="TaskTitle" placeholder="Task Title Here"/></td>
        </tr>
        <tr>
          <td>Time</td>
          <td>
           <select name="TaskHour">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
           </select>
           <select name="TaskMin">
            <option value="00">00</option>
            <option value="05">05</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
            <option value="55">55</option>
           </select>
           <select name="TaskAP">
            <option value="a">AM</option>
            <option value="p">PM</option>
           </select>
          </td>
        </tr>
        <tr>
          <td>Description</td>
          <td><textarea name="TaskDescription" id="TaskDescription" placeholder="Task Description Here"></textarea></td>
        </tr>
        <tr>
          <td>Alarm</td>
          <td><select name="AlarmSettings">
               <option value="0">Set Alarm to above time</option>
               <option value="1">Set Alarm to another time</option>
               <option value="2">Don't set Alarm</option>
              </select>
          </td>
        </tr>
        <tr class="set-alarm">
          <td>Alarm Time</td>
          <td>
           <select name="AlarmHour">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
           </select>
           <select name="AlarmMin">
            <option value="00">00</option>
            <option value="05">05</option>
            <option value="10">10</option>
            <option value="15">15</option>
            <option value="20">20</option>
            <option value="25">25</option>
            <option value="30">30</option>
            <option value="35">35</option>
            <option value="40">40</option>
            <option value="45">45</option>
            <option value="50">50</option>
            <option value="55">55</option>
           </select>
           <select name="AlarmAP">
            <option value="a">AM</option>
            <option value="p">PM</option>
           </select>
          </td>
        </tr>
        <tr class="set-alarm">
			<td>Alarm Date</td><td><input type="text" id="alarm-date-picker" readonly="readonly"/></td>
        </tr>
        <tr>
          <td></td>
          <td>
              <input type="button" name="AddTask" value="Add Task" onclick="addTask()"/>
          </td>
        </tr>
    </table>
  </form>
  <span id="add-task-status">Task Added Successfully</span>
  </div>
  <!-- end of add-task-box -->
</div>
 <!-- end of task-wrapper -->
</div>	
 </div>	
<div style="clear:both;"></div>
</div> <!-- end of tabs -->

<script src="js/center.js"></script>
<script> 


$(document).ready(function(){
	
	var accordion = $('#task-wrapper').accordion();
	var tabs = $('#tabs').tabs();
	tabs.find(".ui-tabs-nav").sortable({
			axis:"x",
			stop:function(){
					tabs.tabs("refresh");
				
				}
		
		});
	
	$('[class=set-alarm]').hide();
	
	$('[name=AlarmSettings]').change(function(){
			if($(this).val()=='1'){
				
				$('[class=set-alarm]').show();
				$("#alarm-date-picker").datepicker({dateFormat:"yy-mm-dd"});
				}
			else{
				$('[class=set-alarm]').hide();
				}
		});
      

  
  $(".modal").center();
  var top = $(".modal").css("top");
  $(".modal").css("maxHeight",(window.innerHeight-top)+"px");
  
  
  $(this).keyup(function(e){
		  if(e.keyCode== 27){
				  if($('.modal-overlay').css('dispay')!='none'){
					  //$('.modal-overlay').hide();
					  $('.modal-overlay').css('display','none');
					  
				  }
			  }
	  });
	  
	 
	   	 
  
});

(function($) {
   $.fn.fixMe = function() {
      return this.each(function() {
         var $this = $(this),
            $t_fixed;
         function init() {
            $this.wrap('<div class="scroll_tbl" />');
            $t_fixed = $this.clone();
            $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
            resizeFixed();
         }
         function resizeFixed() {
            $t_fixed.find("th").each(function(index) {
               $(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
            });
         }
         function scrollFixed() {
            var offset = $(this).scrollTop(),
            tableOffsetTop = $this.offset().top,
            tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
            if(offset < tableOffsetTop || offset > tableOffsetBottom)
               $t_fixed.hide();
            else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
               $t_fixed.show();
         }
         $(".tasklist").resize(resizeFixed);
         $(".tasklist").scroll(scrollFixed);
         init();
      });
   };
})(jQuery);

$(document).ready(function(){
  $("#list_view").fixMe();

	
});
</script>




