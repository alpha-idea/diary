<table id="list_view" border="0">
  <thead>
    <tr>
      <th scope="col">Task</th>
      <th scope="col">Description </th>
      <th scope="col">Time </th>
      <th scope="col">Alarm</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Task 1</td>
      <td>description 1</td>
      <td>Time 1</td>
    </tr>
    <!--  
    <tr class="even">
      <td>Task 2</td>
      <td>description 2</td>
      <td>Time 2</td>
    </tr>
    <tr>
      <td>Task 3</td>
      <td>description 3</td>
      <td>Time 3</td>
    </tr>
    <tr class="even">
      <td>Task 4</td>
      <td>description 4</td>
      <td>Time 4</td>
    </tr>
    <tr>
      <td>Task 5</td>
      <td>description 5</td>
      <td>Time 5</td>
    </tr>
    
    -->
  </tbody>
</table>

<style>
#list_view{
	font-family: Sans-Serif;
	font-size: 14px;
	margin: 45px;
	width: 400px;
	text-align: left;
	border-collapse: collapse;
}
#list_view th{
	font-size: 16px;
	font-weight: normal;
	padding: 10px 8px;
	color: #000;
	text-shadow: 0px 1px 0px #fff;
	background: #fcfcfc; 
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZjZmNmYyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iI2U1ZTVlNSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjZGNkY2QiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #fcfcfc 0%, #e5e5e5 50%, #cdcdcd 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#fcfcfc), color-stop(50%,#e5e5e5), color-stop(100%,#cdcdcd));
	background: -webkit-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	background: -o-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%); 
	background: -ms-linear-gradient(top,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	background: linear-gradient(to bottom,  #fcfcfc 0%,#e5e5e5 50%,#cdcdcd 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfcfc', endColorstr='#cdcdcd',GradientType=0 );
	border: 1px solid #fff;
}
#list_view tr{
	color: #000;
	text-shadow: 0px 1px 0px #fff;
}
#list_view td{
	padding: 8px;
	border: 1px solid #fff;
}
#list_view .even{
	background: #DFDFDF; 
}
#list_view tr:hover{
	background: #ced6df;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2NlZDZkZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iI2MyY2VkYiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNiNmM2ZDciIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #ced6df 0%, #c2cedb 50%, #b6c6d7 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ced6df), color-stop(50%,#c2cedb), color-stop(100%,#b6c6d7));
	background: -webkit-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: -o-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: -ms-linear-gradient(top,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%); 
	background: linear-gradient(to bottom,  #ced6df 0%,#c2cedb 50%,#b6c6d7 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ced6df', endColorstr='#b6c6d7',GradientType=0 );
}

/* ******************************************************** Black Theme ******************************************************** */
/*
#list_view{
	font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
	font-size: 14px;
	margin: 45px;
	width: 400px;
	text-align: left;
	border-collapse: collapse;
}
#list_view th{
	font-size: 16px;
	font-weight: normal;
	padding: 10px 8px;
	color: #FFF;
	background: #9d9d9d;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzlkOWQ5ZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzgwODA4MCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM2NTY1NjUiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #9d9d9d 0%, #808080 50%, #656565 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#9d9d9d), color-stop(50%,#808080), color-stop(100%,#656565)); /
	background: -webkit-linear-gradient(top,  #9d9d9d 0%,#808080 50%,#656565 100%);
	background: -o-linear-gradient(top,  #9d9d9d 0%,#808080 50%,#656565 100%);
	background: -ms-linear-gradient(top,  #9d9d9d 0%,#808080 50%,#656565 100%);
	background: linear-gradient(to bottom,  #9d9d9d 0%,#808080 50%,#656565 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#9d9d9d', endColorstr='#656565',GradientType=0 );
	border: 1px solid #555;
}
#list_view tr{
	color: #ddd;
	background: #414141;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzQxNDE0MSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iIzM5MzkzOSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMzMTMxMzEiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #414141 0%, #393939 50%, #313131 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#414141), color-stop(50%,#393939), color-stop(100%,#313131)); 
	background: -webkit-linear-gradient(top,  #414141 0%,#393939 50%,#313131 100%); 
	background: -o-linear-gradient(top,  #414141 0%,#393939 50%,#313131 100%); 
	background: -ms-linear-gradient(top,  #414141 0%,#393939 50%,#313131 100%); 
	background: linear-gradient(to bottom,  #414141 0%,#393939 50%,#313131 100%); 
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#414141', endColorstr='#313131',GradientType=0 );
}
#list_view td{
	padding: 8px;
	border: 1px solid #555;
	border-radius: 5px;
}
#list_view .even{
	background: #f6bb33; 
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2Y2YmIzMyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjUwJSIgc3RvcC1jb2xvcj0iI2QxNzQxNSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjMjQyMDUiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #f6bb33 0%, #d17415 50%, #c24205 100%); 
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#f6bb33), color-stop(50%,#d17415), color-stop(100%,#c24205));
	background: -webkit-linear-gradient(top,  #f6bb33 0%,#d17415 50%,#c24205 100%); 
	background: -o-linear-gradient(top,  #f6bb33 0%,#d17415 50%,#c24205 100%); 
	background: -ms-linear-gradient(top,  #f6bb33 0%,#d17415 50%,#c24205 100%);
	background: linear-gradient(to bottom,  #f6bb33 0%,#d17415 50%,#c24205 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f6bb33', endColorstr='#c24205',GradientType=0 );
	color: #fff;
}
#list_view tr:hover{
	background: #74d369;
	background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzc0ZDM2OSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMyNGM5MWMiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
	background: -moz-linear-gradient(top,  #74d369 0%, #24c91c 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#74d369), color-stop(100%,#24c91c));
	background: -webkit-linear-gradient(top,  #74d369 0%,#24c91c 100%);
	background: -o-linear-gradient(top,  #74d369 0%,#24c91c 100%); 
	background: -ms-linear-gradient(top,  #74d369 0%,#24c91c 100%);
	background: linear-gradient(to bottom,  #74d369 0%,#24c91c 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#74d369', endColorstr='#24c91c',GradientType=0 );
	color: #ffffff !important;
}*/
</style>
