<?php
include_once('../SessionHandler.php');
include_once('../source/Diary.class.php');

$entries;
$diary = new Diary();
if(isset($_REQUEST['Today'])) {
			//echo $_REQUEST['Today'];
		 $dateArray =  explode('-', $_REQUEST['Today']);
			
		if(checkdate($dateArray[1], $dateArray[2], $dateArray[0])){
			
				$entries = $diary->getEntriesForTheDay($_REQUEST['Today']);			
			
			}	
	
	}
if(isset($entries) && !empty($entries)) {
	
	foreach($entries as $entry){

?>

<div <?php echo 'id="dpost_'.$entry->getEntryId().'"'; ?> class="dpost_boxes">

 <div class="dpost_mini">
  <div class="dpostTxt">
   		<?php 
				echo $entry->getEntry();   		
   		?>
  </div>
 </div>
   <span class="postTime">@ <?php echo $entry->getUserTime(); ?></span>
</div>

<?php 
	}

}
?>