<?php
include_once('SessionHandler.php');
include_once('source/Diary.class.php');

$curuser = new CurrentUser();
///echo $curuser->getCurrentUserId();
$displayName = $curuser->getFirstName()." ".$curuser->getLastName();
$likes = $curuser->getLikes();
$diary = new Diary();

$entries = $diary->getRecentEntriesOfUser();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>*** User ***</title>
<link href="style/profile.css" rel="stylesheet" type="text/css">
<link href="style/perfect-scrollbar.css" rel="stylesheet">
<link rel="stylesheet" href="style/greenbar.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="js/jquery-1.9.1.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script src="js/profile.js"></script>
<script type="text/javascript">
  $(document).ready(function ($) {
	$('#rightScrollCon').perfectScrollbar({
	  wheelSpeed: 20,
	  wheelPropagation: false
	});
  });
</script>
</head>

<body>
  <div id="diaryCon">
   <div id="diary">
    <div id="leftPage">
     <div id="leftBar">
      <div id="profileImg"><img src="images/pro_m.png" alt="User Image" title="User Image"></div>
      <div id="totLikes"><p>Likes</p><span class="boxVal"><?php echo $likes; ?></span></div>
      <div id="totPosts"><p>Posts</p><span class="boxVal">9999</span></div>
      <div id="totContacts"><p>Contacts</p><span class="boxVal">9999</span></div>
      <div id="totFollows"><p>Followers</p><span class="boxVal">9999</span></div>
     </div>
     <div id="leftContent">
      <span id="uName"><?php echo $displayName; ?></span>
      
      <table width="355" border="0">
        <tr>
          <td rowspan="5"><div class="proInLab">Address</div></td>
          <td><div class="proInfo">7, Carlisle terres,</div></td>
        </tr>
        <tr>
          <td><div class="proInfo">st neots</div></td>
        </tr>
        <tr>
          <td><div class="proInfo">Cambridge</div></td>
        </tr>
        <tr>
          <td><div class="proInfo">PE25 7HG</div></td>
        </tr>
        <tr>
          <td><div class="proInfo">United Kingdom</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Birthday</div></td>
          <td><div class="proInfo">16/08/1989</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Email</div></td>
          <td><div class="proInfo">kavindaps@gmail.com</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Mobile Phone</div></td>
          <td><div class="proInfo">0094715586852</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Home Phone</div></td>
          <td><div class="proInfo">0094112921188</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Business Phone</div></td>
          <td><div class="proInfo">0094112921188</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Office Phone</div></td>
          <td><div class="proInfo">0094112921188</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Fax Number</div></td>
          <td><div class="proInfo">0094112921188</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Driving Licence</div></td>
          <td><div class="proInfo">zxcvbn</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Passport Number</div></td>
          <td><div class="proInfo">w3456yuj</div></td>
        </tr>
        <tr>
          <td><div class="proInLab">Blood Group</div></td>
          <td><div class="proInfo">B+</div></td>
        </tr>
      </table>
      
     </div>
    </div>
    <div id="rightPage">
    <!-- enrty start -->
     <div id="rightScrollCon">
<!-- -->
<?php 

		foreach ($entries as $entry){
			//error_log(print_r($entry,true));
?>
       <div class="diaryEntry">
        <div class="entryDate">
         <div class="dateNum"><?php echo strftime("%d",strtotime($entry->getEntryDate())); ?></div>
         <div class="dateDay"><?php echo strftime("%A",strtotime($entry->getEntryDate())); ?></div>
        </div>
        <div class="likeBtn">
			
					<img id='entry_star' src="images/gstar.png" alt="liked">
			
		</div>
       <div class="likeNum"><span><?php echo $entry->getLikes(); ?></span></div>
        <div class="entryContent">
			<p>
				<?php echo $entry->getEntry();  ?>
						<!-- ENTRY CONTENT -->
			</p>
        </div>
       </div>
       
<!-- -->
<?php 
	
}
			
?>
     </div>
     <!-- enrty end -->
     </div>
    </div>
  </div>
<!---->
<?php include('html/greenbar.html'); ?>
</body>
</html>
