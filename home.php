<?php
include_once('SessionHandler.php');
include_once('source/Diary.class.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>*** Home ***</title>

<link rel="stylesheet" href="style/home.css">
<link rel="stylesheet" href="style/greenbar.css">
<link rel="stylesheet" href="style/clndr.css">
<link rel="stylesheet" href="style/diary.css">
<link href="style/perfect-scrollbar.css" rel="stylesheet">
<link rel="stylesheet" href="style/popuptop.css"> <!-- diary entry top bar -->
<link rel="stylesheet" href="style/popdisplay.css"> <!--display old posts -->

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>

<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>

<script src="js/json2.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="js/moment-2.2.1.js"></script>
<script src="js/datetime.js"></script>
<script src="js/comet.js"></script>
<script src="js/profile.js"></script>
<script src="js/clndr.js"></script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
var calendars = {};
var eventArray ;
var entryData = new Object({date:"",visibility:"Private"});
  $(document).ready(function ($) {
/******************************************************************************************************/	  
	  $.ajax({url:"events.php",
		dataType:"json",
		cache:false,
		async:false,
		data:{yyyy_mm:moment().format('YYYY-MM')}, 
		success:function(data,status){
			console.log("success");
			console.log(data);
			eventArray = data;
			
			},
		error:function(a,b){
				console.log(a);
				console.log(b);
			}
	});
	console.log(eventArray);
	 calendars.clndr1 = $('.cal1').clndr({
    events: eventArray,
    // constraints: {
    //   startDate: '2013-11-01',
    //   endDate: '2013-11-15'
    // },
    clickEvents: {
      click: function(target) {
        //console.log(target);
      viewDayPopup(target);
        
        // if you turn the `constraints` option on, try this out:
        // if($(target.element).hasClass('inactive')) {
        //   console.log('not a valid datepicker date.');
        // } else {
        //   console.log('VALID datepicker date.');
        // }
      },
      nextMonth: function() {
        console.log('next month.');
      },
      previousMonth: function() {
        
      },
      onMonthChange: function() {
        
        
        console.log(this.month.format('YYYY-MM'));
		$.ajax({url:"events.php",
		dataType:"json",
		//cache:false,
		async:false,
		data:{yyyy_mm:this.month.format('YYYY-MM')}, 
		success:function(data,status){
			console.log(data);
			eventArray = data;
			
			}
	});
     console.log(eventArray);
      this.setEvents(eventArray);
        
        
      },
      nextYear: function() {
        console.log('next year.');
      },
      previousYear: function() {
        console.log('previous year.');
      },
      onYearChange: function() {
        console.log('year changed.');
      }
    },
    multiDayEvents: {
      startDate: 'startDate',
      endDate: 'endDate'
    },
    forceSixRows:true,
    showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
  });
/******************************************************************************************************/	  
	$('#leftScrollCon').perfectScrollbar({
	  wheelSpeed: 20,
	  wheelPropagation: false
	});
	
	$('#diaryEntry').perfectScrollbar({
	  wheelSpeed: 20,
	  wheelPropagation: false
	});
/******************************************************************************************************/	
$('.cal2').hide();
$('#fullDate').click(function(){$('.cal2').slideDown();});
$('.cal2').mouseout(function(){$(this).slideUp();});
$('.cal2').focusout(function(){$(this).hide();});
/******************************************************************************************************/	

$("#diary_goto_date").val(getFormattedToday());
$("#diary_goto_date").datepicker({dateFormat:'yy-mm-dd',
												defaultDate: 0,
												onSelect:function(date, inst){
															
															loadDiary(date);
															setEntryDate(new Date(date)); //in datetime.js
															
											}});

setInterval(function(){ connect(); }, 15000);
		
		
  $(this).keyup(function(e){
		  if(e.keyCode== 27){
				  if($('.modal-overlay').css('dispay')!='none'){
					  
					  $('.modal-overlay').css('display','none');
					  
				  }
			  }
	  });		
		
  }); //end Doc Ready...

function getFormattedToday(){

					var d = new Date();

					var month = d.getMonth()+1;
				var day = d.getDate();

				var outputdate = d.getFullYear() + '-' +
    			(month<10 ? '0' : '') + month + '-' +
    			(day<10 ? '0' : '') + day;
			return outputdate;
	}
</script>

<!-- JQUERY UI -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">

<link rel="stylesheet" href="style/modal.css" type="text/css">

<script src="js/dayPopup.js"></script>
<script src="js/home.js"></script>

<script src="ckeditor/ckeditor.js"></script>

</head>

<body>
	<div id="diaryCon">
		<div id="diary">
        	<div id="leftPage">
             <div id="leftScrollCon">
				 
				 <?php
					$diary = new Diary();
					$entries = $diary->getRecentEntries();
					foreach($entries as $entry){
						if($entry->getEntryVisibility()=='public'){
                ?>
				 
				 
               <div class="diaryEntry">
               <div class="dlCon">
                <div class="entryDate">
                 <div class="dateNum"><?php echo strftime("%d",strtotime($entry->getEntryDate())); ?></div>
                 <div class="dateDay"><?php echo strftime("%A",strtotime($entry->getEntryDate())); ?></div>
                </div>
                <div class="likeBtn">
					<?php if(!$diary->hasLiked($entry->getEntryId())){ ?>
					<a href="#" onclick='event.preventDefault();likeEntry(<?php echo $entry->getEntryId();?>);'><img id='entry_star' src="images/emptystar.png" alt="click to like"></a>
					<?php  }else{ ?>
					<img id='entry_star' src="images/gstar.png" alt="liked">
					<?php } ?>
				</div>
                <div class="likeNum"><span><?php echo $entry->getLikes(); ?></span></div>
                </div>
                
                <div class="entryContent">
                <?php echo $entry->getEntry();  ?>
                </div>
               </div>
               
               <?php 
					}
               }
                ?>
             </div>
            </div>
            <div id="rightPage">
             <div class="cal1"></div>
             <div id="featBoxCon">
              <a href="#" class="featBox red">Contacts</a>
              <a href="#" class="featBox teal">Friends</a>
              <a href="#" class="featBox green">To Do</a>
              <a href="#" class="featBox pink">Profile</a>
              <a href="#" class="featBox blue">Events</a>
              <a href="#" class="featBox green" onclick="event.preventDefault();loadDiary(getFormattedToday());setEntryDate(new Date());">Note</a>
              <a href="#" class="featBox orange">Tasks</a>
              <a href="#" class="featBox purple">Manager</a>
             </div>
            </div>
        </div>
	</div>
	
	<!--
	MODEL WINDOW DIARY 
	-->
	 <div class="modal-overlay diaryModal">

				<div id="topbarContent">
 <div id="topBar">
  <ul>
   <li><a href="#">Go to Date</a><input type="text" id="diary_goto_date" readonly="readonly"></input></li>
   <li><a href="#">Visibility</a><select onchange="entryData.visibility=this.value;"><option value="Private">Private</option><option value="Public">Public</option></select></li>
   <li><a href="#" onclick="event.preventDefault();saveEntry();">Save</a></li>
   <li><div id="clock"><span id="hours"> </span><span id="point">:</span><span id="min"> </span><span id="point">:</span><span id="sec"> </span></div></li>
  </ul>
 </div>
</div>

		
		
		<div id="modal" class="modal">
			
			<a href='#' onclick="event.preventDefault();$('.modal-overlay').hide();">
			
			<img src='images/button_cancel.png' class='close-box'/>
			</a>
		
				<div id="fullDate"><div id="Date"></div></div>
				<div id="item-1">
				
				</div>
				<div id="item-2">
						
						          <div id="diaryEntry">
										<div class="page" contenteditable="true">
										<p>Dear Diary,</p>
										</div>
									</div>
						
				</div>
				<div id="item-3"></div>
				
		</div>
  </div>
	<!--
	DIARY MODEL WINDOW END
	-->
	
<!--
	MODEL WINDOW TASK 
	-->	
	 <div class="modal-overlay taskModal">
		<div id="modal" class="modal">
			
			<a href='#' onclick="event.preventDefault();$('.modal-overlay').hide();">
			
			<img src='images/button_cancel.png' class='close-box'/>
			</a>
				
				<div id="item-1">
				
				</div>
				<div id="item-2">
						
				</div>
				<div id="item-3"></div>
				
		</div>
	
	</div>	
	<!--
	TASK MODEL WINDOW END
	-->
	
	<!--
	GLOBAL BAR
	-->
	<?php include('html/greenbar.html'); ?>
</body>

<script type="text/javascript" >
function loadDiary(date){
				
				if(date==""){
						
								date = getFormattedToday();	
								staticDate = date;				
								setEntryDate(new Date(date)); //in datetime.js
					}				
				entryData.date = date;
				$('#item-1').load('popup/old-post.php',{Today:date},function(){});
				
				$('.diaryModal').show();
				
				
		}

function saveEntry(){
				var time = hours+":"+minutes+":"+seconds;
				var content = $("#diaryEntry .page").html();
				//entryDate.date;
				//	
				//alert(entryData.visibility);			
				$.ajax({
					
							url:'DiaryHandler.php',
							//dataType:'json',
							type:'POST',
							async:false,
							data:{UserTime:time, EntryText:content, EntryVisibility:entryData.visibility, EntryDate:entryData.date,diary_add:1},
							success:function(data,status){
								//alert('test');
										
										loadDiary(entryData.date);										
										$("#diaryEntry .page").text("");					
								}					
					
					});
	}
	
function loadComments(){
	
		
	}
	
function saveComment(){
				var usertime = hours+":"+minutes+":"+seconds;
				var comment /* = $("#diaryEntry .page").html() */;
				var commentdate;
					
							
				$.ajax({
					
							url:'DiaryHandler.php',
							//dataType:'json',
							type:'POST',
							async:false,
							data:{UserTime:usertime, CommentText:comment, CommentDate:commentdate,comment_add:1},
							success:function(data,status){
								//alert('test');
										
										loadComments(entryData.date);										
										//$("#diaryEntry .page").text("");					
								}					
					
					});
	}
</script>
</html>
