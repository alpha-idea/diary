CKEDITOR.plugins.add('save',{
	
		init: function(editor){
			
				editor.addCommand('saveEntry',
				{
					exec: function (editor){
							
							var timestamp = new Date();
							editor.insertHtml(timestamp.toString());
						
						}
				});
				
				
				//editor.ui.addToolbarGroup('diary',0);
				editor.ui.addRichCombo('visiblility',{
						label:"Entry Visibility",
						title:"Entry Visibility is Private by default",
						voiceLabel: 'Set Entry Visibility',
						className: 'cke_format',
						multiSelect:false,
						panel:{
								css:[editor.config.contentsCss, CKEDITOR.skin.getPath('editor')],
								voiceLabel: editor.lang.panelVoiceLabel
							},
						init:function(){
													
							this.add('private','Private','Visibility - Only You');
							this.add('public','Public','Visibility - Your Friends & Followers');
						},
						onClick:function(value){
						
								//setVisibility();
								//alert(value);
								
						}
						
					});
				editor.ui.addButton('Save',{
				
					label:"Save Diary Entry",
					command:'saveEntry',
								
				});
			}
	
	});
