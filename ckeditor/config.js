﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
	config.plugins = 'dialogui,dialog,a11yhelp,basicstyles,blockquote,clipboard,panel,floatpanel,menu,contextmenu,resize,button,toolbar,elementspath,enterkey,entities,popup,filebrowser,floatingspace,listblock,richcombo,format,horizontalrule,htmlwriter,wysiwygarea,image,indent,indentlist,fakeobjects,link,list,magicline,maximize,pastetext,pastefromword,removeformat,sourcearea,specialchar,menubutton,scayt,stylescombo,tab,table,tabletools,undo,wsc,font,lineutils,widget,mathjax,autosave,panelbutton,colorbutton,find,save'; //gg removed
	config.skin = 'moonocolor';
	// %REMOVE_END%
	config.filebrowserBrowseUrl ='ckeditor/filemanager/browser/default/browser.html?Connector=http://localhost/webs/diary/ckeditor/filemanager/connectors/php/connector.php';
	config.filebrowserImageBrowseUrl = 'ckeditor/filemanager/browser/default/browser.html?Type=Image&Connector=http://localhost/webs/diary/ckeditor/filemanager/connectors/php/connector.php';
	config.filebrowserFlashBrowseUrl ='ckeditor/filemanager/browser/default/browser.html?Type=Flash&Connector=http://localhost/webs/diary/ckeditor/filemanager/connectors/php/connector.php';
	config.filebrowserUploadUrl  ='http://localhost/webs/diary/ckeditor/filemanager/connectors/php/upload.php?Type=File';
	config.filebrowserImageUploadUrl = 'http://localhost/webs/diary/ckeditor/filemanager/connectors/php/upload.php?Type=Image';
	config.filebrowserFlashUploadUrl = 'http://localhost/webs/diary/ckeditor/filemanager/connectors/php/upload.php?Type=Flash';
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection'/*, 'spellchecker' */] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others'},
		{ name: 'save'},
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	
	config.disableNativeSpellChecker = false;
	
	//config.extraPlugins='save';
};
