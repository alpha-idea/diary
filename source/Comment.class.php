<?php

include_once('Diary.class.php');

class Comment extends Diary{

	protected $CommentId;
	protected $CommentText;
	protected $CommentDate;
	
	protected $tableMapComments = array(
	
				'CommentId' => 'id',
				'EntryId' => 'entryid',
				'UserId' => 'userid',
				'UserTime' => 'usertime',
				'CommentText' => 'comment',
				'CommentDate'  => 'entrydate',
			
		);		


		public function addNewComment(){
		
			R::begin();
			
			try{
				//adding to user table
				$comment = R::dispense('comments');
				
				foreach($this->tableMapComments as $key=>$value)
				{
					if($key=='EntryId'){
						if(isset($this->EntryId)){					
						$comment->entryid = $this->EntryId;
						}												
						continue;
						}
						
					if($key=='UserId'){
						if(isset($this->CurrentUserId)){					
						$comment->userid = $this->CurrentUserId;
						}												
						continue;
						}
					if(!isset($this->$key)){
					
						eval('$this->set'.$key.'(null, true) ;');
						
					}
					
					$comment->$value =	$this->$key;
					
				}
				
								
				//$comment->userid = $this->CurrentUserId;
					
				
				$id = R::store($comment);
				
				$this->CommentId = $id;
				
				R::commit();
				
			
			}
			catch(Exception $e){
					R::rollback();
					echo "error $e";
					
										
					
				
				}
	
		}	// END OF ADD NEW COMMENT




	public function getCommentText() {
		}

	public function setCommentText($CommentText, $post=true){
			
			
			if($post && isset($_POST['CommentText'])){
				 
					$this->CommentText = ($_POST['CommentText']);
				 }
			if(isset($CommentText)){	 
				$this->CommentText = $CommentText;
			}
		
		}

	public function getCommentDate() {
		}

	public function setCommentDate($CommentDate, $post=true){
			
			
			if($post && isset($_POST['CommentDate'])){
				 
					$this->CommentDate = ($_POST['CommentDate']);
				 }
			if(isset($CommentDate)){	 
				$this->CommentDate = $CommentDate;
			}
		
		}
				
/********************************************************ADD LIKES********************/


}//END OF THE CLASS comment

?>