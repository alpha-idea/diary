<?php
class Alarm{
	
		protected $AlarmTime;
		protected $AlarmDate;
		protected $AlarmType;
		private $CurrentUserId;
	
		private $tableMap = array(
								"AlarmTime"=>"alarm_time",
								 "AlarmDate" =>"alarmDate",
								);
	
		public function __construct(){
			
				$cu = new CurrentUser();
				$this->CurrentUserId = $cu->getCurrentUserId();
			}
	
		public function isAlarmSet($TaskId){
			
				$id = R::$f->begin()->select('id')->from('alarm')->where('userid=?')->put($CurrentUserId)->get('row');
				if(isset($id[0]['id'])){
					
						return true;
					
					}
				return false;

			}
			
		public function setAlarm($AlarmDate, $AlarmTime){
			$alarm =  R::dispense('alarm');
			$alarm->alarm_date = $AlarmDate;
			$alarm->alarm_time = $AlarmTime;
			$alarm->userid = $this->CurrentUserId;
			$alarm->alarm_types_id = 1; //testing /**************************/
			
			foreach($this->tableMap as $key=>$value){
					
					if(!isset($this->$key)){
						//	return 0;
						}
				
				}
			
			$AlarmId = R::store($alarm);
			if(isset($AlarmId) && $AlarmId>0){
					return $AlarmId;
				}
				else{
					return 0;
					}
			
			}
			
		public function getAlarmType(){
			
			
			}
		
		public function setAlarmType(){
			
			
			}
		public function setAlarmTime($AlarmTime){
				$this->AlarmTime = $AlarmTime;
			
			}
			
		public function setAlarmDate($AlarmDate){
				$this->AlarmDate = $AlarmDate;
			
			}
			
		public function getAlarmDate(){
				
			
			}
			
		public function changeAlarmDate(){
			
			
			}
			
		public function changeAlarmTime(){
			
			
			}
		
		public function snooze(){
			
			
			
			}
			
	
	}
?>
