<?php //require('rbconnection.php');
//include_once('CurrentUser.class.php');
//include_once('Alarm.class.php');
include_once('model/Task.model.php');
	/*FIELS OF THE TABLE TASKS
	 * 
	 * id 
	 * tasktitle 
	 * taskdesc 
	 * taskdate 
	 * User_id
	 * 
	 * 
	 */ 

class Task implements Updateable, Selectable{
	protected $TaskId; 
	private $TaskInstance;
	protected $TaskTitle;
	protected $TaskDescription;
	protected $TaskDate;
	protected $TaskTime;
	protected $TaskMin;
	protected $TaskHour;
	protected $TaskAP;
	private $CurrentUserId;
	//protected $AlarmTime;
	//protected $AlarmDate;
	
	protected $AlarmId;
	
	protected $userid;
	
	protected $tableMap = array(
				'TaskId' => 'id',
				//'UserId' => 'userid',
				'TaskTitle' => 'tasktitle',
				'TaskDescription' => 'taskdesc',
				'TaskDate'  => 'taskdate',
				'TaskTime' => 'tasktime',
				
		);	
	
	public function __construct($TaskId=null){
		
				if(is_null($TaskId)){
						$this->TaskId = null;
					
					}
				else if(isset($TaskId)){
					
						$this->TaskId = $TaskId;
						$this->TaskInstance = R::load('tasks',$TaskId);
						
					}
				$cu = new CurrentUser();
				$this->CurrentUserId = $cu->getCurrentUserId();
				
		}
	public function addNewTask(){
		
			R::begin();
			
			try{
				//adding to user table
				$task = R::dispense('tasks');
				
				foreach($this->tableMap as $key=>$value)
				{
					if($key=='TaskId'){continue;}
					if(!isset($this->$key)){
					
						eval('$this->set'.$key.'(null, true) ;');
						
					}
					
					$task->$value =	$this->$key;
					
					}
				
				$task->userid = $this->CurrentUserId;
					
				
				$id = R::store($task);
				
				$this->TaskId = $id;
				
				R::commit();
				
			
			}
			catch(Exception $e){
					R::rollback();
					echo "error $e";
					
										
					
				
				}
				
			
		
		
		}
		
		/*
		 * 
		 * @yyyy_mm year and month in  "yyyy-mm" format
		 * @return : returns a _Task object array
		 * * 
		 * 
		 * 
		 */ 
		
	public function getTasksForTheMonth($yyyy_mm=null){
		if(!isset($this->CurrentUserId) || is_null($this->CurrentUserId)){
				return null;
			}
				$tasks = array();
				if(!is_null($yyyy_mm)){
							
							$tasks = $this->select("*", "userid = :userid AND taskdate LIKE :taskdate", array(":userid"=>$this->CurrentUserId, ":taskdate"=>"$yyyy_mm%"));
							
							
					
					}
				if(!empty($tasks)){
							
						//print_r($tasks);
						$index = 0;
						$taskArray = null;
						foreach($tasks as $task){
					
						
						$_task = new _Task();
						$_task->setTaskId($task['id']);
						$_task->setTaskTitle($task['tasktitle']);
						$_task->setTaskDescription($task['taskdesc']);
						$_task->setTaskDate($task['taskdate']);
						$_task->setTaskTime($task['tasktime']);
						$taskArray[$index] = $_task;
						$index++;
				
						}	
			
					
					return $taskArray;
					
					
					
					}
					
					return null;
		
		}	
	/*
	 * 
	 * 
	 *@return : returns a _Task object array
	 * 
	 * 
	 */ 
	public function getTasksForTheDay($TaskDate=null){
			$date = null;
			if(!is_null($TaskDate)){
					
					$date = $TaskDate;
				
				}
			else{
					if(isset($this->TaskDate)){
						
								$date = 	$this->TaskDate;				
						}
				}
				
			$tasks = $this->select("*", "taskdate=:date AND userid=:userid ORDER BY tasktime", array(':date'=>$date,':userid'=>$this->CurrentUserId));
			
			//print_r($tasks);
			$index = 0;
			$taskArray = null;
			foreach($tasks as $task){
					
						
						$_task = new _Task();
						$_task->setTaskId($task['id']);
						$_task->setTaskTitle($task['tasktitle']);
						$_task->setTaskDescription($task['taskdesc']);
						$_task->setTaskDate($task['taskdate']);
						$_task->setTaskTime($task['tasktime']);
						$taskArray[$index] = $_task;
						$index++;
				
				}
			return $taskArray;
		}
	
	public function editTask(){
		
		
		}
		
		
	public function deleteTask(){
			R::begin();
			$task = R::load('tasks',$this->TaskId);
			
			try{
			 R::trash($task);
			 return true;
			}catch(Exception $e){
						return false;
				}
			
			
		
		}
		
		
	public function getTaskId(){}
	public function setTaskId(){}
	
	public function getTaskTitle(){}
	public function setTaskTitle($TaskTitle, $post=true){
			
			
			if($post && isset($_POST['TaskTitle'])){
				 
					$this->TaskTitle = ($_POST['TaskTitle']);
				 }
			if(isset($TaskTitle)){	 
				$this->TaskTitle = $TaskTitle;
			}
		
		}
	
	public function getTaskDescription(){}
	public function setTaskDescription($TaskDescription, $post=true){
			
			
			if($post && isset($_POST['TaskDescription'])){
				 
					$this->TaskDescription = ($_POST['TaskDescription']);
				 }
			if(isset($TaskDescription)){	 
				$this->TaskDescription = $TaskDescription;
			}
		
		}
	
	
	public function getTaskTime(){
		
		
		}
	public function setTaskTime($TaskTime, $post=true){
			
			
			if($post && isset($_POST['TaskTime'])){
				 
					$this->TaskTime = ($_POST['TaskTime']);
				 }
			if(isset($TaskTime)){	 
				$this->TaskTime = $TaskTime;
			}
		
		}
	
	public function setTaskDate($TaskDate, $post=true){
			
			
			if($post && isset($_POST['TaskDate'])){
				 
					$this->TaskDate = ($_POST['TaskDate']);
				 }
			if(isset($TaskDate)){	 
				$this->TaskDate = $TaskDate;
			}
		
		}
	
	
	public function isAlarmSet($TaskId){
			
				
				if(isset($id[0]['id'])){
					
						return true;
					
					}
				return false;

			}
			
	/*
	 * @where Parameter binding eg:- title = :title
	 * @values An Array containing values for the parameters bound eg array(':title'=>'home')
	 * @return a multidimentional array with rows
	 */ 		
	public function select($select,$where, $values){
		
				//return R::$f->begin()->select($select)->from('tasks')->where($where)->put($values)->get();
				
				return R::getAll("SELECT $select FROM tasks WHERE $where", $values);
					
		} 		
	public function setAlarm($AlarmDate=null, $AlarmTime=null){
			if(isset($this->TaskId) && !is_null($this->TaskId)){
				$alarm  = new Alarm();
				$AlarmId = 0;
				if(isset($this->TaskDate) && isset($this->TaskTime))
					{
						if(!is_null($AlarmDate) && !is_null($AlarmTime))
						{
							$AlarmId = $alarm->setAlarm($AlarmDate, $AlarmTime);
						}
						else{
							$AlarmId = $alarm->setAlarm($this->TaskDate, $this->TaskTime);
							}
							
						if(isset($AlarmId) && $AlarmId>0)	{
							
							R::exec("UPDATE tasks SET alarmid=$AlarmId WHERE id=$this->TaskId");
														
							}
					}
			}
			
		
		}
	
	public function update(){
			
			
		}
	}
?>
