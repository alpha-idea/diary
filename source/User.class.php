<?php session_start();
//include_once('../SessionHandler.php');
require_once('RedBean/rbconnection.php');
//include_once($_SERVER['DOCUMENT_ROOT'].'/diary'.'/SessionHandler.php');
//require_once(__RB__.'/rbconnection.php');
/*** FIELDS OF THE TABLE USER
userid, 
firstname, 
middlename, 
lastname, 
email, 
password, 
address, 
gender, 
birthday, 
profilepic, 
datecreated, 
lastlogin, 
lastactive, 
mobile_phone, 
business_phone, 
home_phone, 
fax_number, 
driving_licence,
passport_num, 
bloodgrp
likes
******/

/*** FIELDS OF THE TABLE EMAIL
 * 
 * email
 * verify_code
 * verified
 * User_id
 * email_type
 * 
 */
 
/*****MODEL CLASS OF THE TABLE USER****/
class User implements Updateable, Selectable{
	
	
protected $UserId; 
protected $FirstName;
protected $MiddleName;
protected $LastName;
protected $Email; //this is same for both user and email tables
protected $AdditionalEmail; 
protected $Password; 
protected $Address; 
protected $Gender; 
protected $Birthday; 
protected $Profilepic; 
protected $Datecreated;
protected $LastLogin; 
protected $LastActive;
protected $MobilePhone;
protected $BusinessPhone;
protected $HomePhone; 
protected $FaxNumber; 
protected $DrivingLicence;
protected $PassportNum;
protected $Bloodgrp;		

protected $VerifyCode;
protected $EmailType;
protected $Verified;

private $UserInstance;

protected $tableMap = array(
				'UserId' => 'id',
				'FirstName' => 'firstname',
				'MiddleName' => 'middlename',
				'LastName' => 'lastname',
				'Email' => 'email',
				'Password' => 'password',
				'Address' => 'address',
				'Gender' => 'gender',
				'Birthday' => 'birthday',
				'MobilePhone' => 'mobile_phone',
				'BusinessPhone' => 'business_phone',
				'HomePhone' => 'home_phone',
				'FaxNumber' => 'fax_number',
				'DrivingLicence' => 'driving_licence',
				'PassportNum' => 'passport_num',
				'Bloodgrp' => 'bloodgrp',
				
		);	
		
		
	public function __construct($UserId=null){
		/*
				if(isset($Email) && filter_var($Email, FILTER_VALIDATE_EMAIL))
				 
				  {
					  
					   $this->Email = $Email;
					   $this->setUserId();
					   $this->setUserIns( $this->Email);
					}
				else if(isset($_POST['Email']) && filter_var($_POST['Email'], FILTER_VALIDATE_EMAIL)){
						
						$this->Email = $_POST['Email'];
						$this->setUserId();
					    $this->setUserIns($this->Email);
					}
		*/
		
				if(isset($UserId))
				 
				  {					  
					   $this->UserId = $UserId;
					   $this->setUserIns();
					   
					}
				else if(isset($_POST['UserId'])){
						
						$this->UserId = $_POST['UserId'];
						$this->setUserIns();
											    
					}
					
				error_log("user id is ".$this->UserId." end");
		}
		
		


	public function setUserIns($Email=null){
			$id = null;
			if(!is_null($Email)){
				
					$id = $this->getUserId($Email);
				
				}
			else{
				
					$id = $this->UserId;
				
				}
			
			if(!is_null($id)){
				
				$this->UserInstance = R::load('user',$id);
				error_log("here 1".$this->UserId );
			}
			else{
				
				$this->UserInstance = null;
				}
	}
	

	

	protected function matchSessionId(){
		
			
			$id = R::getAll("SELECT userid FROM sessions WHERE session_id=:sessid", array(":sessid"=>session_id()));	
			//$id = $this->select('id','sessionid=?',session_id());
			error_log("in matchSessionId function ".print_r($id,true));
			
			if(!empty($id))
				{	
				
						error_log(print_r($id,true));
						return $id[0]['userid'];
				}
				
				return null;
			
		}	

	public function getUserIns(){
		
			return $this->UserInstance;
			
	}	
	
	public function getUserId($Email){
		
			$id = R::$f->begin()->select('id')->from('user')->where('email=?')->put($Email)->get('row');
			if(isset($id)){
			return $id['id'];
			}
			return null;
		}	
		/*
		 * 
		 * 
		 * set the UserId property by getting the Email.
		 * 
		 */ 
	public function setUserId(){
		
			$id = R::$f->begin()->select('id')->from('user')->where('email=?')->put($this->Email)->get('row');
			
			if(isset($id)){
			$this->UserId = $id['id'];
			}
			else{
			$this->UserId = null;
			}
		}	
		
	public function addNewUser(){
			R::begin();
			$id = null;
			try{
				//adding to user table
				$user = R::dispense('user');
				
				foreach($this->tableMap as $key=>$value)
				{
					if($key=='UserId'){continue;}
					eval('$this->set'.$key.'(null, true) ;');
					
					
					$user->$value =	$this->$key;
					
					}
				
				
					
				if($this->Email != null){
				$email = R::dispense('email'); 
				$this->SetVerifyCode(1);
				
				$email->email = $this->Email;
				$email->verify_code = $this->VerifyCode;
				
				
				$id = R::store($user);
				$email->userid =  $id;
				
				
				
				R::store($email);
				R::commit();
				
				include_once('Emailer.php');
				$url = 'http://www.innovativewebarchitects.com/diary/';
				$url .= rawurlencode("verifyEmail.php");
				$url .= "?PrimaryEmail=".urlencode($this->Email);
				$url .= "&Code=".urlencode($this->VerifyCode);
				$message = "<a href='$url'>Please Click Here To Verify Your Email Address</a>";
				emailer($this->Email,"New User", "Email Verification", $message) ; // this funtion is in Emailer.php
				
				}
				else{
						return;
					}
			}
			catch(Exception $e){
					R::rollback();
					echo "error $e";
					R::trash(R::load('user',$id));
										
					
				
				}
				
		}
		
	/*
	 * @Email: Unique identifier to Update rows
	 * set email field to unique
	 **/
	
	public function update(){
										
			$user = $this->getUserIns();
			
			if(!is_null($this->AdditionalEmail)){
				$email = R::dispense('email');
				
				$email->email = $this->AdditionalEmail;
				$this->setVerifyCode(1);
				$email->verify_code = $this->VerifyCode;
				$email->userid = $this->UserId;
				
				R::store($email);	
				$this->AdditionalEmail = null;
			}
		
			
			if(isset($user)){
			foreach($this->tableMap as $key=>$value)
				{
					if($key=='UserId'){continue;}
					if(isset($_POST)){
					eval('$this->set'.$key.'(null, true) ;');
					}
					if(!is_null($this->$key)){
						$user->$value =	$this->$key;
					}
					
					
					}
				try{
					
				R::store($user);	
				//echo "updated";	
				}
				catch(exception $e){
					
						echo "error Updating ".$e;
					}
				
			}
			else{
					echo "entered email not found";
				
				}
		}
	
	
		
	/*
	 * @Email: Unique identifier to Delete rows
	 * set email field to unique
	 **/
	
	public function delete($Email){
			
			$id = R::$f->begin()->select('id')->from('user')->where('email=?')->put($Email)->get('row');
			if(isset($id)){
			$user = R::load('user',$id['id']);
			
		
				R::trash($user);	
			}
			else{
					echo "entered email not found";
				
				}
		}
	
	/*
	 *@select: the returning filed. pass '*' for all fields. fields in the user table are : userid, firstname, middlename, lastname, email, password, address, gender, 
	 *birthday, profilepic, datecreated, lastlogin, lastactive, mobile_phone, business_phone, home_phone, fax_number, driving_licence,passport_num, bloodgrp
	 *@where: field(s) from the table user eg- $where = "email=? AND address LIKE ?"
	 *@values: value to search for (this must match with the 'where' parameter) eg- $value = "'shandanjay@gmail.com', 'Colombo'"
	 *@return: returns a multi dimentional array of data('*'=data of all fields) which are matching to the criteria 
	 */
	public function select($select,$where,$values){
		
			return R::$f->begin()->select($select)->from('user')->where($where)->put($values)->get();
		
		}
		
		
	/*
	 *
	 * @return: returns an array of matching results where considering all the members have set. 
	 */
	public function search(){
			
		//not implemented yet
		}
		
	private function getEmailId($Email){
			$id = R::$f->begin()->select('id')->from('email')->where('email=?')->put($Email)->get('row');
			//print_r($id);
			return $id['id'];
		}
	
	public function getFirstName(){
			if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		}
		
		
	private function get($property){
		
			$user = R::load('user', $this->UserId);
			$field = $this->tableMap[$property];
			return $user->$field;
		
		
		}	
		
	public function setFirstName($FirstName, $post=true){
			
			
			if($post && isset($_POST['FirstName'])){
				 
					$this->FirstName = ($_POST['FirstName']);
				 }
			if(!is_null($FirstName)){
					$this->FirstName = $FirstName;
			}
			
		}
		
	public function getMiddleName(){
		
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		}
	
	public function setMiddleName($MiddleName, $post=true){
			
			if($post && isset($_POST['MiddleName']))
			{
				 
					$this->MiddleName = ($_POST['MiddleName']);
			 }
			if(isset($MiddleName))
			{
				$this->MiddleName = $MiddleName;
			}
		}
	
	
			
	public function getLastName(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
	
	public function setLastName($LastName, $post=true){
			
			
			if($post && isset($_POST['LastName'])){
				 
					$this->LastName = ($_POST['LastName']);
				 }
			if(isset($LastName)){	 
				$this->LastName = $LastName;
			}
		}
	
		public function getEmail(){
			return $this->Email;
		
		}
	
	public function setEmail($Email, $post=true){
			
			if($post && isset($_POST['Email'])){
					
					$email = ($_POST['Email']);
					if(filter_var($email, FILTER_VALIDATE_EMAIL)) {$this->Email = $email;}
					else {$this->Email = null;}
				 }
			if(isset($Email)){
				
				if(filter_var($Email, FILTER_VALIDATE_EMAIL)) {$this->Email = $Email;}
				else {$this->Email = null;}
				
			}
		}
	public function setAdditionalEmail($Email, $post=true){
		
			if($post && isset($_POST['AdditionalEmail'])){
					
					$email = ($_POST['AdditionalEmail']);
					if(filter_var($email, FILTER_VALIDATE_EMAIL)) {$this->AdditionalEmail = $email;}
					else {$this->AdditionalEmail = null;}
				 }
			if(isset($Email)){
				
				if(filter_var($Email, FILTER_VALIDATE_EMAIL)) {$this->AdditionalEmail = $Email;}
				else {$this->AdditionalEmail = null;}
				
			}
		
		}	
	/*
	 * Email & Password properties must be set before calling this function
	 * @return true if success
	 */ 	
   private function comparePasswords(){
			if(isset($this->Email) && isset($this->Password))
			{
				
				$result = R::$f->begin()->select('id')->from('user')->where("password=?")->put($this->Password)->get();
				
				if(isset($result) && isset($result[0]['id']) && $result[0]['id']!=""){
						
						return true;
						
				}
				else{
						//echo "Password or Email not matching";
						return false;
					}
			}
		}
	
	public function setPassword($Password, $post=true){
			if($post && isset($_POST['Password']) && isset($this->Email)){
				 
					$this->Password = $this->hashPasswd($this->Email,($_POST['Password']));
				 }
			if(isset($Password) && isset($this->Email)) {
					$this->Password = $this->hashPasswd($this->Email,$Password);
			}
		}
	
	private function hashPasswd($Email, $Password){
		
			 $hashed = crypt($Email,$Password); 
			 return $hashed;
		
		}	
		
	   public function getPassportNum(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
	
	
		public function setPassportNum($PassportNum, $post=true){
			if($post && isset($_POST['PassportNum'])){
				 
					$this->PassportNum = ($_POST['PassportNum']);
				 }
			if(isset($PassportNum)) {
			$this->PassportNum = $PassportNum;
			}
		}
		
	public function getAdress(){
			
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}	
		
	public function setAddress($Address, $post=true){
			if($post && isset($_POST['Address'])){
				 
					$this->Address = ($_POST['Address']);
				 }
			if(isset($Address)) {
			$this->Address = $Address;
			}
		}
		
    public function getBirthday(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
	
	public function setBirthday($Birthday, $post=true){
			if($post && isset($_POST['Birthday'])){
				 
					$this->Birthday = ($_POST['Birthday']);
				 }
			if(isset($Birthday)) {
			$this->Birthday = $Birthday;
			}
		}	
		
	public function getGender(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
	
	public function setGender($Gender, $post=true){
			if($post && isset($_POST['Gender'])){
				 
					$this->Gender = ($_POST['Gender']);
				 }
			if(isset($Gender)) {
			$this->Gender = $Gender;
			}
		}		
		
	
	public function getMobilePhone(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
		
	public function setMobilePhone($MobilePhone, $post=true){
			if($post && isset($_POST['MobilePhone'])){
				 
					$this->MobilePhone = ($_POST['MobilePhone']);
				 }
			if(isset($MobilePhone)) {
			$this->MobilePhone = $MobilePhone;
			}
		}	
		
	public function getBusinessPhone(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
		
	public function setBusinessPhone($BusinessPhone, $post=true){
			if($post && isset($_POST['BusinessPhone'])){
				 
					$this->BusinessPhone = ($_POST['BusinessPhone']);
				 }
			if(isset($BusinessPhone)) {
			$this->BusinessPhone = $BusinessPhone;
			}
		}	
		/////////
			public function getHomePhone(){
			return $this->HomePhone;
		
		}
		
	public function setHomePhone($HomePhone, $post=true){
			if($post && isset($_POST['HomePhone'])){
				 
					$this->HomePhone = ($_POST['HomePhone']);
				 }
			if(isset($HomePhone)) {
			$this->HomePhone = $HomePhone;
			}
		}	
		
	public function getFaxNumber(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
		
		}
		
	public function setFaxNumber($FaxNumber, $post=true){
			if($post && isset($_POST['FaxNumber'])){
				 
					$this->FaxNumber = ($_POST['FaxNumber']);
				 }
			if(isset($FaxNumber)) {
			$this->FaxNumber = $FaxNumber;
			}
		}	
	public function getDrivingLicence(){
			return $this->DrivingLicece;
		
		}
		
		
		
	public function setDrivingLicence($DrivingLicence, $post=true){
			if($post && isset($_POST['DrivingLicence'])){
				 
					$this->DrivingLicence = ($_POST['DrivingLicence']);
				 }
			if(isset($DrivingLicence)) {
			$this->DrivingLicence = $DrivingLicence;
			}
		}	
	public function getBloodgrp(){
						if(isset($this->UserId)){
				$value = $this->get(substr(__FUNCTION__, 3));
				if(isset( $value )){
					
					return $value;
					
					}
					
			}									
			return null;
			
		}	
		
	public function setBloodgrp($Bloodgrp, $post=true){
			if($post && isset($_POST['Bloodgrp'])){
				 
					$this->Bloodgrp = ($_POST['Bloodgrp']);
				 }
			if(isset($Bloodgrp)) {
			$this->Bloodgrp = $Bloodgrp;
			}
		}	
	public function emailValidate(){
			
			if(isset($this->VerifyCode) && ($this->VerifyCode!="verified")){
				
				if(isset($this->AdditionalEmail)){
					
						if($this->VerifyCode==$this->getVerifyCode($this->AdditionalEmail))
						{
									
									$email = R::load('email', $this->getEmailId($this->AdditionalEmail));
									$email->verified = '1';
									$email->verify_code = "verified";
									R::store($email);
							
						}					
				}
				else{
					
					//echo $this->getVerifyCode($this->Email);
						if($this->VerifyCode==$this->getVerifyCode($this->Email))
						{
							
								$email = R::load('email', $this->getEmailId($this->Email));
								$email->verified = '1';
									$email->verify_code = "verified";
									R::store($email);
									//echo $this->Email . " verified";
							
						}
					
					}
					
			
				
				
			}
		
		}
	public function getVerifyCode($Email){
							
			$code = R::$f->begin()->select('verify_code')->from('email')->where("email=?")->put($Email)->get();
			if(isset($code)){
				
				return $code[0]['verify_code'];
				}
			return null;
		}	
		
	/*
	 * 
	 *@return: generated md5 verification code
	 * 
	 */ 	
	public function setVerifyCode($code)
		{
		
			if($code == 1)
			{
				$this->VerifyCode = md5(uniqid(rand()));
			}
			else if(is_null($code)){
				
					$this->VerifyCode = "verified";
				}
			else{
				
					$this->VerifyCode = $code;
				}
		}	
		
	public function getVerified(){
		
		}
		
	public function setVerified($verify=false){
			
				$this->Verified = $verify;
		}
	/*
	 * 
	 * user login function
	 * this checks whether the primary email is verified and email+password = hash
	 * 
	 * 
	 */ 	
	public function login(){
				
			$this->setEmail(null,true);
			$this->setPassword(null, true);

			if($this->isPrimaryEmailVerified() && $this->comparePasswords()){
				$this->setUserId();
				//error_log("start session_id = ".session_id());
				$this->storeSessionId(session_id());
				//$_SESSION['userid'] = $this->UserId;
				//echo "loggedin ";
				return true;
				}
				return false;		
		}
		
	private function isPrimaryEmailVerified(){
		
			$verified = R::$f->begin()->select('verified')->from('email')->where("email=?")->put($this->Email)->get();
			
			if(isset($verified) && !empty($verified)){
				
			if($verified[0]['verified']==1){
					
					return true;
				}
			}
			return false;
		}
		
	public function checkEmailVerified($Email){
		
			$verified = R::$f->begin()->select('verified')->from('email')->where("email=?")->put($Email)->get();
			if($verified[0]['verified']==1){
					
					return true;
				}
		
		}
		
	public function logout(){
			
			$this->removeSessionId();
									
					//$this->storeSessionId("logout");
					//$this->removeSessionId();
					session_unset();
					session_destroy();
					//echo "loggedout ";
			
		}
		
	/*
	 * 
	 * 
	 *@return: array of session_id
	 * 
	 */ 	
	protected function getSessionId(){
				$session_ids;
				if(isset($this->UserId)){
				//$user = $this->getUserIns();
				//$sessionid = $user->sessionid;
				 $session_ids = R::getAll("SELECT session_id FROM sessions WHERE userid=:userid", array(":userid"=>$this->UserId));	
				 error_log("in get Session Id function ".print_r($session_ids,true));
				return $session_ids;
				}
				return null;
		}
	
	
		
	private function storeSessionId($sess_id){
			
			//$user = $this->getUserIns();
			//$user->sessionid = $sess_id;
			//R::store($user);
			try{
			R::begin();
			$session = R::dispense('sessions');
			$session->session_id  = $sess_id;
			$session->userid = $this->UserId;
			R::store($session);
			R::commit();
			}
			catch(Exception $e){
				error_log("may be session_id exists");
					error_log($e);
				
				}
		}
	private function removeSessionId(){
			
			//$user = $this->getUserIns();
			//$user->sessionid = $sess_id;
			//R::store($user);
			
			$session_ids = R::getAll("SELECT id FROM sessions WHERE userid=:userid", array(":userid"=>$this->UserId));	
			
			foreach($session_ids as $sessionsid){
				//echo $sessionsid['id'];
				error_log($sessionsid['id']);
			$session = R::load('sessions',$sessionsid['id']);
			R::trash($session);
			
			}
			
		}	
		
	public function getLikes(){
				if(isset($this->UserId) && !is_null($this->UserId))
				{
					error_log("first name". $this->UserInstance->firstname);
					return $this->UserInstance->likes;
					
				}
		}
	public function setLike(){
				if(isset($this->UserId) && !is_null($this->UserId))
				{
					$likes = $this->getLikes();
					$likes++;
					$UserInstance->likes =	$likes;
					R::store($UserInstance);				
				}		
		}	

}//END CLASS USERS


?>
