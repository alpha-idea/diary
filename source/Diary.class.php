<?php

//require('rbconnection.php');
include_once('CurrentUser.class.php');
//include('Alarm.class.php');
include_once('model/Entry.model.php');
	/*FIELS OF THE TABLE entries
	 * 
	 * id 
	 * entrytitle 
	 * entrydesc 
	 * entrydate 
	 * entryvisibility
	 * User_id
	 * likes
	 * 
	 */ 

class Diary implements Updateable, Selectable{
	protected $EntryId;
	
	private $EntryInstance;
	
	protected $EntryText;
	protected $EntryDate;
	protected $EntryTime;
	protected $UserTime;
	protected $EntryVisibility;
	
	private $CurrentUserId;
	//protected $AlarmTime;
	//protected $AlarmDate;
		
	protected $userid;
	
	protected $tableMap = array(
				'EntryId' => 'id',
				//'UserId' => 'userid',
				'UserTime' => 'usertime',
				'EntryText' => 'entry',
				'EntryDate'  => 'entrydate',
				'EntryVisibility' => 'entryvisibility',
				
				
		);
		

	
	public function __construct($EntryId=null){
		
				if(is_null($EntryId)){
						$this->EntryId = null;
					
					}
				else if(isset($EntryId)){
					
						$this->EntryId = $EntryId;
						$this->EntryInstance = R::load('entries',$EntryId);
						
					}
				$cu = new CurrentUser();
				$this->CurrentUserId = $cu->getCurrentUserId();
				
		}
	public function addNewEntry(){
		
			R::begin();
			
			try{
				//adding to user table
				$entry = R::dispense('entries');
				
				foreach($this->tableMap as $key=>$value)
				{
					if($key=='EntryId'){continue;}
					if(!isset($this->$key)){
					
						eval('$this->set'.$key.'(null, true) ;');
						
					}
					
					$entry->$value =	$this->$key;
					
				}
				
				if(isset($this->EntryVisibility) && $this->EntryVisibility=='Public'){ //capital P 
					
					$entry->entryvisibility = 'public';
				}
				
				$entry->userid = $this->CurrentUserId;
					
				
				$id = R::store($entry);
				
				$this->EntryId = $id;
				
				R::commit();
				
			
			}
			catch(Exception $e){
					R::rollback();
					echo "error $e";
					
										
					
				
				}
	
		}
	


	
	public function autoSave(){
			
			foreach($this->tableMap as $key=>$value)
				{
					if($key=='EntryId'){continue;}
					if(!isset($this->$key)){
					
						eval('$this->set'.$key.'(null, true) ;');
						
					}
					
					//$entry->$value =	$this->$key;
					
				}
				//error_log($this->EntryText." ".$this->EntryDate);
				//$entry->userid = $this->CurrentUserId;
				R::exec('INSERT INTO entries(entry , entrydate, userid) VALUES ("'.
						$this->EntryText.'","'.
						$this->EntryDate.'",'.
						$this->CurrentUserId.') '.
						'ON DUPLICATE KEY UPDATE entry=VALUES(entry)'
						);
		
		}
		
	public function SaveEntry()	{
		
		
		
		
		
		}
		/*
		 * 
		 * @yyyy_mm year and month in  "yyyy-mm" format
		 * @return : returns a _Entry object array
		 * * 
		 * 
		 * 
		 */ 
		
	public function getEntriesForTheMonth($yyyy_mm=null){
		if(!isset($this->CurrentUserId) || is_null($this->CurrentUserId)){
				return null;
			}
				$entries = array();
				if(!is_null($yyyy_mm)){
							
							$entries = $this->select("*", "userid = :userid AND entrydate LIKE :Entrydate", array(":userid"=>$this->CurrentUserId, ":entrydate"=>"$yyyy_mm%"));
							
							
					
					}
				if(!empty($entries)){
							
						print_r($entries);
						$index = 0;
						$EntryArray = null;
						foreach($entries as $Entry){
					
						
						$_Entry = new _Entry();
						$_Entry->setEntryId($Entry['id']);
						$_Entry->setEntryTitle($Entry['Entrytitle']);
						$_Entry->setEntryDescription($Entry['Entrydesc']);
						$_Entry->setEntryDate($Entry['Entrydate']);
						$_Entry->setEntryTime($Entry['Entrytime']);
						$EntryArray[$index] = $_Entry;
						$index++;
				
						}	
			
					
					return $EntryArray;
					
					
					
					}
					
					return null;
		
		}	
	/*
	 * 
	 * 
	 *@return : returns entries for the day for a selected user
	 * 
	 * 
	 */ 
	public function getEntriesForTheDay($EntryDate=null){
			$date = null;
			if(!is_null($EntryDate)){
					
					$date = $EntryDate;
				
				}
			else{
					if(isset($this->EntryDate)){
						
								$date = 	$this->EntryDate;				
						}
				}
				
			$entries = $this->select("*", "entrydate=:date AND userid=:userid", array(':date'=>$date,':userid'=>$this->CurrentUserId));
			
			error_log("Entry Array for the day ".print_r($entries,true));
			
			if(empty($entries)){
					return "";
				}		
			
			
			$_entryArray;	
			$count = 0;
			foreach($entries as $entry){
			
				$_entry = new _Entry();
				$_entry->setEntryId($entries[$count]['id']);
				$_entry->setEntry($entries[$count]['entry']);
				$_entry->setEntryDate($entries[$count]['entrydate']);
				$_entry->setEntryTime($entries[$count]['entrytime']);
				$_entry->setUserTime($entries[$count]['usertime']);
				$_entry->setEntryVisibility($entries[$count]['entryvisibility']);
				$_entry->setLikes($entries[$count]['likes']);
				$_entry->setUserId($entries[$count]['userid']);
				$_entryArray[$count] = $_entry;
				$count++;
			}			
			//error_log("Entry Array of user ".print_r($_entryArray,true));
			return $_entryArray;
		}
		
		/*
	 * 
	 * 
	 *@return : returns a _Entry object array for a user
	 * 
	 * 
	 */ 
	public function getRecentEntriesOfUser($UserId=null){
			$entries = "";
			if(isset($this->CurrentUserId)){
			if(!is_null($UserId)){
			
				$entries = $this->select("*", "userid=:userid ORDER BY entrydate DESC LIMIT 6", array(':userid'=>$UserId));
			
			//error_log("Entry Array of user ".print_r($entries,true));
			}
			else {
				
				$entries = $this->select("*", "userid=:userid ORDER BY entrydate DESC LIMIT 6", array(':userid'=>$this->CurrentUserId));	
				}
			if(empty($entries)){
					return "";
				}
			$_entryArray;	
			$count = 0;
			foreach($entries as $entry){
			
				$_entry = new _Entry();
				$_entry->setEntryId($entries[$count]['id']);
				$_entry->setEntry($entries[$count]['entry']);
				$_entry->setEntryDate($entries[$count]['entrydate']);
				$_entry->setEntryTime($entries[$count]['entrytime']);
				$_entry->setUserTime($entries[$count]['usertime']);
				$_entry->setEntryVisibility($entries[$count]['entryvisibility']);
				$_entry->setLikes($entries[$count]['likes']);
				$_entry->setUserId($entries[$count]['userid']);
				$_entryArray[$count] = $_entry;
				$count++;
			}			
			//error_log("Entry Array of user ".print_r($_entryArray,true));
			return $_entryArray;
			}
			return null;
		}
		
			/*
	 * 
	 * 
	 *@return : returns a _Entry object array for all the recent entries
	 * 
	 * 
	 */ 
	public function getRecentEntries(){
			$entries = "";
			
			if(isset($this->CurrentUserId)){
			
				$entries = $this->select("id,substr(entry,1,200) as entry, entrydate, entrytime,entryvisibility,likes", " entryvisibility=:visibility ORDER BY entrydate DESC LIMIT 6", array(':visibility'=>'public'));
			
			//error_log("Entry Array of user ".print_r($entries,true));
			}
			if(empty($entries)){
					return "";
				}
			$_entryArray;	
			$count = 0;
			foreach($entries as $entry){
			
				$_entry = new _Entry();
				$_entry->setEntryId($entries[$count]['id']);
				$_entry->setEntry($entries[$count]['entry']);
				$_entry->setEntryDate($entries[$count]['entrydate']);
				$_entry->setEntryTime($entries[$count]['entrytime']);
				$_entry->setEntryVisibility($entries[$count]['entryvisibility']);
				$_entry->setLikes($entries[$count]['likes']);
				$_entryArray[$count] = $_entry;
				$count++;
			}			
			//error_log("Entry Array of user ".print_r($_entryArray,true));
			return $_entryArray;
		}
	public function editEntry(){
		
		
		}
		
		
	public function deleteEntry(){
			R::begin();
			$entry = R::load('entries',$this->EntryId);
			
			try{
			 R::trash($entry);
			 return true;
			}catch(Exception $e){
						return false;
				}
			
			
		
		}
		
		
	public function getEntryId(){
			return $this->EntryId;
		}
	public function setEntryId(){}
	

	
	public function getEntryText(){
			return $this->EntryText;
		}
		
	public function setEntryText($EntryText, $post=true){
			
			
			if($post && isset($_POST['EntryText'])){
				 
					$this->EntryText = ($_POST['EntryText']);
				 }
			if(isset($EntryText)){	 
				$this->EntryText = $EntryText;
			}
		
		}
	
	
	public function getEntryTime(){
		
			return $this->EntryTime;
		
		}
	public function setEntryTime($EntryTime, $post=true){
			
			
			if($post && isset($_POST['EntryTime'])){
				 
					$this->EntryTime = ($_POST['EntryTime']);
				 }
			if(isset($EntryTime)){	 
				$this->EntryTime = $EntryTime;
			}
		
		}
	
	public function getEntryDate(){
		
			return $this->EntryDate;
		}
	
	public function setEntryDate($EntryDate, $post=true){
			
			
			if($post && isset($_POST['EntryDate'])){
				 
					$this->EntryDate = ($_POST['EntryDate']);
				 }
			if(isset($EntryDate)){	 
				$this->EntryDate = $EntryDate;
			}
		
		}

///////////

	public function getUserTime(){
		
			return $this->UserTime;
		}
	
	public function setUserTime($UserTime, $post=true){
			
			
			if($post && isset($_POST['UserTime'])){
				 
					$this->UserTime = ($_POST['UserTime']);
				 }
			if(isset($UserTime)){	 
				$this->UserTime = $UserTime;
			}
		
		}




	public function getEntryVisibility(){
		
			return $this->EntryVisibility;
		}
	
	public function setEntryVisibility($EntryVisibility, $post=true){
						
			if($post && isset($_POST['EntryVisibility'])){
				 
					$this->EntryVisibility = ($_POST['EntryVisibility']);
				 }
			if(isset($EntryVisibility)){	 
				$this->EntryVisibility = $EntryVisibility;
			}
		
		}

	public function getLikes(){
				if(isset($this->EntryId) && !is_null($this->EntryId))
				{
					
					return $this->EntryInstance->likes;
					
				}
		}
	public function setLike(){
				if(isset($this->EntryId) && !is_null($this->EntryId))
				{
					$likes = $this->getLikes();
					$likes++;
					$this->EntryInstance->likes =	$likes;
					R::store($this->EntryInstance);				
				}		
		}
		
	public function hasLiked($EntryId=null){
			
			if(is_null($EntryId)){
				$EntryId = $this->EntryId;
			}
				$result = R::getCell('SELECT id FROM entrylikes WHERE entryid='.$EntryId.' AND userid='.$this->CurrentUserId);
				if(is_null($result)){
					
						return false;
					}
				return true;
		
		}
		
	public function setLiked(){
			
			$like = R::dispense('entrylikes');
			$like->userid = $this->CurrentUserId;
			$like->entryid = $this->EntryId;
			$likeid = R::store($like);
			
			if(is_null($likeid)){
				
					return false;
				}
			else{
					return true;
				
				}
		
		
		}
	/*
	 * @where Parameter binding eg:- title = :title
	 * @values An Array containing values for the parameters bound eg array(':title'=>'home')
	 * @return a multidimentional array with rows
	 */ 		
	public function select($select,$where, $values){
		
				//return R::$f->begin()->select($select)->from('Entrys')->where($where)->put($values)->get();
				
				return R::getAll("SELECT $select FROM entries WHERE $where", $values);
					
		} 		
	/*
	 * 
	 *this function updates the fields of the table entries. Ensure that, the initialized with the id and the properties are set, before calling. 
	 * 
	 * 
	 */ 
	
	public function update(){
			
			if(isset($this->EntryId)){
				
				if(isset($this->EntryText) || isset($this->EntryDate)|| isset($this->EntryTime) || isset($this->EntryVisibility)){
					$entry = R::load('entries',$this->EntryId);
					if(isset($this->EntryText)){
						$entry->entry = $this->EntryText;
						}
					if(isset($this->EntryDate)){
						$entry->entrydate = $this->EntryDate;
						}
						
					if(isset($this->UserTime)){
						$entry->entrytime = $this->EntryTime;
						}
					if(isset($this->EntryVisibility)){
						$entry->entryvisibility = $this->EntryVisibility;
						}
					
					R::store($entry);
				}
				
			}
			
		}
	}
?>
