<?php

class Contact_{
	
	
  private $ContactTd;
  private $Prefix;
  private $FirstName;
  private $LastName;
  private $HomePhone;
  private $WorkPhone;
  private $MobilePhone;
  private $PrefName;
  private $Gender;
  private $Birthday;
  private $AdrNum;
  private $Street;
  private $City;
  private $County;
  private $PostCode;
  private $Country;
  private $Email;
  private $Fax;
  private $Website;
  private $SkypeId;
  private $FbName;
  private $TwitterName;
  private $Gplus;
  private $Youtube;
	
	private $userid;
	
	
	
	
	private $tableMap;
	
	
	
	public function getContactTd() {
            return $this->ContactTd;
        }

        public function setContactTd($ContactTd) {
            $this->ContactTd = $ContactTd;
        }

        public function getPrefix() {
            return $this->Prefix;
        }

        public function setPrefix($Prefix) {
            $this->Prefix = $Prefix;
        }

        public function getFirstName() {
            return $this->FirstName;
        }

        public function setFirstName($FirstName) {
            $this->FirstName = $FirstName;
        }

        public function getLastName() {
            return $this->LastName;
        }

        public function setLastName($LastName) {
            $this->LastName = $LastName;
        }

        public function getHomePhone() {
            return $this->HomePhone;
        }

        public function setHomePhone($HomePhone) {
            $this->HomePhone = $HomePhone;
        }

        public function getWorkPhone() {
            return $this->WorkPhone;
        }

        public function setWorkPhone($WorkPhone) {
            $this->WorkPhone = $WorkPhone;
        }

        public function getMobilePhone() {
            return $this->MobilePhone;
        }

        public function setMobilePhone($MobilePhone) {
            $this->MobilePhone = $MobilePhone;
        }

        public function getPrefName() {
            return $this->PrefName;
        }

        public function setPrefName($PrefName) {
            $this->PrefName = $PrefName;
        }

        public function getGender() {
            return $this->Gender;
        }

        public function setGender($Gender) {
            $this->Gender = $Gender;
        }

        public function getBirthday() {
            return $this->Birthday;
        }

        public function setBirthday($Birthday) {
            $this->Birthday = $Birthday;
        }

        public function getAdrNum() {
            return $this->AdrNum;
        }

        public function setAdrNum($AdrNum) {
            $this->AdrNum = $AdrNum;
        }

        public function getStreet() {
            return $this->Street;
        }

        public function setStreet($Street) {
            $this->Street = $Street;
        }

        public function getCity() {
            return $this->City;
        }

        public function setCity($City) {
            $this->City = $City;
        }

        public function getCounty() {
            return $this->County;
        }

        public function setCounty($County) {
            $this->County = $County;
        }

        public function getPostCode() {
            return $this->PostCode;
        }

        public function setPostCode($PostCode) {
            $this->PostCode = $PostCode;
        }

        public function getCountry() {
            return $this->Country;
        }

        public function setCountry($Country) {
            $this->Country = $Country;
        }

        public function getEmail() {
            return $this->Email;
        }

        public function setEmail($Email) {
            $this->Email = $Email;
        }

        public function getFax() {
            return $this->Fax;
        }

        public function setFax($Fax) {
            $this->Fax = $Fax;
        }

        public function getWebsite() {
            return $this->Website;
        }

        public function setWebsite($Website) {
            $this->Website = $Website;
        }

        public function getSkypeId() {
            return $this->SkypeId;
        }

        public function setSkypeId($SkypeId) {
            $this->SkypeId = $SkypeId;
        }

        public function getFbName() {
            return $this->FbName;
        }

        public function setFbName($FbName) {
            $this->FbName = $FbName;
        }

        public function getTwitterName() {
            return $this->TwitterName;
        }

        public function setTwitterName($TwitterName) {
            $this->TwitterName = $TwitterName;
        }

        public function getGplus() {
            return $this->Gplus;
        }

        public function setGplus($Gplus) {
            $this->Gplus = $Gplus;
        }

        public function getYoutube() {
            return $this->Youtube;
        }

        public function setYoutube($Youtube) {
            $this->Youtube = $Youtube;
        }


	}


?>
