<?php

class _Task{
	
	private $TaskId; 
	private $TaskTitle;
	private $TaskDescription;
	private $TaskDate;
	private $TaskTime;
	private $TaskMin;
	private $TaskHour;
	private $TaskAP;
	
	private $AlarmTime;
	private $AlarmId;
	
	private $userid;
	
	private $tableMap = array(
				'TaskId' => 'id',
				//'UserId' => 'userid',
				'TaskTitle' => 'tasktitle',
				'TaskDescription' => 'taskdesc',
				'TaskDate'  => 'taskdate',
				'TaskTime' => 'tasktime',
				
		);	
	
	
	
                public function getTaskId() {
                    return $this->TaskId;
                }

                public function setTaskId($TaskId) {
                    $this->TaskId = $TaskId;
                }

                public function getTaskTitle() {
                    return $this->TaskTitle;
                }

                public function setTaskTitle($TaskTitle) {
                    $this->TaskTitle = $TaskTitle;
                }

                public function getTaskDescription() {
                    return $this->TaskDescription;
                }

                public function setTaskDescription($TaskDescription) {
                    $this->TaskDescription = $TaskDescription;
                }

                public function getTaskDate() {
                    return $this->TaskDate;
                }

                public function setTaskDate($TaskDate) {
                    $this->TaskDate = $TaskDate;
                }

                public function getTaskTime() {
                    return $this->TaskTime;
                }

                public function setTaskTime($TaskTime) {
                    $this->TaskTime = $TaskTime;
                }

                public function getTaskMin() {
                    return $this->TaskMin;
                }

                public function setTaskMin($TaskMin) {
                    $this->TaskMin = $TaskMin;
                }

                public function getTaskHour() {
                    return $this->TaskHour;
                }

                public function setTaskHour($TaskHour) {
                    $this->TaskHour = $TaskHour;
                }

                public function getTaskAP() {
                    return $this->TaskAP;
                }

                public function setTaskAP($TaskAP) {
                    $this->TaskAP = $TaskAP;
                }

                public function getAlarmTime() {
                    return $this->AlarmTime;
                }

                public function setAlarmTime($AlarmTime) {
                    $this->AlarmTime = $AlarmTime;
                }

                public function getAlarmId() {
                    return $this->AlarmId;
                }

                public function setAlarmId($AlarmId) {
                    $this->AlarmId = $AlarmId;
                }

                public function getUserid() {
                    return $this->userid;
                }

                public function setUserid($userid) {
                    $this->userid = $userid;
                }



}


?>
