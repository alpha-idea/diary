<?php

class Contact_{
	
	
  private $ContactTd;
  private $Prefix;
  private $FirstName;
  private $LastName;
  private $HomePhone;
  private $WorkPhone;
  private $MobilePhone;
  private $PrefName;
  private $Gender;
  private $Birthday;
  private $AdrNum;
  private $Street;
  private $City;
  private $County;
  private $PostCode;
  private $Country;
  private $Email;
  private $Fax;
  private $Website;
  private $SkypeId;
  private $FbName;
  private $TwitterName;
  private $Gplus;
  private $Youtube;
	
	private $userid;
	
	
	
	
private $tableMap= array{
  
    'Prefix' => 'prefix',							
   'FirstName'=>'firstname' ,
  'LastName'=> 'lastname' ,
  'HomePhone'=>'homePhone' ,
   'WorkPhone'=>'workPhone' ,
    'MobilePhone'=>'mobilePhone' ,
    'PrefName'=>'prefname' ,
   'Gender'=>'gender' ,
  'Birthday'=>'bday' ,
  'AdrNum'=>'adrnum' ,
   'Street'=>'street' ,
    'City'=>'city' ,
   'County'=>'county' ,
    'PostCode'=>'pocode' ,
  'Country'=>'country' ,
    'Email'=>'email' ,
    'Fax'=>'fax' ,
    'Website'=>'website',
  'SkypeId'=>'skype' ,
  'FbName'=>'fb' ,
  'TwitterName'=>'tw' ,
  'Gplus'=>'gp' ,
  'Youtube' =>'yt' ,
};
	
	
	public function addNewContact(){
			R::begin();
			$id = null;
			try{
				//adding to user table
				$contact = R::dispense('contact');
				
				foreach($this->tableMap as $key=>$value)
				{
					if($key=='UserId'){continue;}
					eval('$this->set'.$key.'(null, true) ;');
					
					
					$contact->$value =	$this->$key;
					
					}
				
				
					
				if($this->Email != null){
				$uc = R::dispense('user_contacts'); 
											
				
				
				$id = R::store($contact);
				$uc->contacts_id =  $id;
				$cu = new CurrentUser();
				$uc->userid = $cu->getCurrentUserId();
				
				
				R::store($uc);
				R::commit();
				}
				else{
						return;
					}
			}
			catch(Exception $e){
					R::rollback();
					echo "error $e";
					R::trash(R::load('contact',$id));
										
					
				
				}
				
		}
		
	
	
	
		public function getContactTd() {
            return $this->ContactTd;
        }

        public function setContactTd($ContactTd) {
            $this->ContactTd = $ContactTd;
        }

        public function getPrefix() {
            return $this->Prefix;
        }

        public function setPrefix($Prefix, $post=true) {
            			
			if($post && isset($_POST['Prefix'])){
				 
					$this->Prefix = ($_POST['Prefix']);
				 }
			if(!is_null($Prefix)){
					$this->Prefix = $Prefix;
			}
        }

        public function getFirstName() {
            return $this->FirstName;
        }

        public function setFirstName($FirstName, $post=true) {
            			
			if($post && isset($_POST['FirstName'])){
				 
					$this->FirstName = ($_POST['FirstName']);
				 }
			if(!is_null($FirstName)){
					$this->FirstName = $FirstName;
			}
        }

        public function getLastName() {
            return $this->LastName;
        }

        public function setLastName($LastName, $post=true) {
           			
			if($post && isset($_POST['LastName'])){
				 
					$this->LastName = ($_POST['LastName']);
				 }
			if(!is_null($LastName)){
					$this->LastName = $LastName;
			}
        }

        public function getHomePhone() {
            return $this->HomePhone;
        }

        public function setHomePhone($HomePhone, $post=true) {
            			
			if($post && isset($_POST['HomePhone'])){
				 
					$this->HomePhone = ($_POST['HomePhone']);
				 }
			if(!is_null($HomePhone)){
					$this->HomePhone = $HomePhone;
			}
        }

        public function getWorkPhone() {
            return $this->WorkPhone;
        }

        public function setWorkPhone($WorkPhone, $post=true) {
            			
			if($post && isset($_POST['WorkPhone'])){
				 
					$this->WorkPhone = ($_POST['WorkPhone']);
				 }
			if(!is_null($WorkPhone)){
					$this->WorkPhone = $WorkPhone;
			}
        }

        public function getMobilePhone() {
            return $this->MobilePhone;
        }

        public function setMobilePhone($MobilePhone, $post=true) {
            			
			if($post && isset($_POST['MobilePhone'])){
				 
					$this->MobilePhone = ($_POST['MobilePhone']);
				 }
			if(!is_null($MobilePhone)){
					$this->MobilePhone = $MobilePhone;
			}
        }

        public function getPrefName() {
            return $this->PrefName;
        }

        public function setPrefName($PrefName, $post=true) {
            			
			if($post && isset($_POST['PrefName'])){
				 
					$this->PrefName = ($_POST['PrefName']);
				 }
			if(!is_null($PrefName)){
					$this->PrefName = $PrefName;
			}
        }

        public function getGender() {
            return $this->Gender;
        }

        public function setGender($Gender, $post=true) {
            			
			if($post && isset($_POST['Gender'])){
				 
					$this->Gender = ($_POST['Gender']);
				 }
			if(!is_null($Gender)){
					$this->Gender = $Gender;
			}
        }

        public function getBirthday() {
            return $this->Birthday;
        }

        public function setBirthday($Birthday, $post=true) {
            			
			if($post && isset($_POST['Birthday'])){
				 
					$this->Birthday = ($_POST['Birthday']);
				 }
			if(!is_null($Birthday)){
					$this->Birthday = $Birthday;
			}
        }

        public function getAdrNum() {
            return $this->AdrNum;
        }

        public function setAdrNum($AdrNum, $post=true) {
            			
			if($post && isset($_POST['AdrNum'])){
				 
					$this->AdrNum = ($_POST['AdrNum']);
				 }
			if(!is_null($AdrNum)){
					$this->AdrNum = $AdrNum;
			}
        }

        public function getStreet() {
            return $this->Street;
        }

        public function setStreet($Street, $post=true) {
            			
			if($post && isset($_POST['Street'])){
				 
					$this->Street = ($_POST['Street']);
				 }
			if(!is_null($Street)){
					$this->Street = $Street;
			}
        }

        public function getCity() {
            return $this->City;
        }

        public function setCity($City, $post=true) {
           			
			if($post && isset($_POST['City'])){
				 
					$this->City = ($_POST['City']);
				 }
			if(!is_null($City)){
					$this->City = $City;
			}
        }

        public function getCounty() {
            return $this->County;
        }

        public function setCounty($County, $post=true) {
            			
			if($post && isset($_POST['County'])){
				 
					$this->County = ($_POST['County']);
				 }
			if(!is_null($County)){
					$this->County = $County;
			}
        }

        public function getPostCode() {
            return $this->PostCode;
        }

        public function setPostCode($PostCode, $post=true) {
            			
			if($post && isset($_POST['PostCode'])){
				 
					$this->PostCode = ($_POST['PostCode']);
				 }
			if(!is_null($PostCode)){
					$this->PostCode = $PostCode;
			}
        }

        public function getCountry() {
            return $this->Country;
        }

        public function setCountry($Country, $post=true) {
           			
			if($post && isset($_POST['Country'])){
				 
					$this->Country = ($_POST['Country']);
				 }
			if(!is_null($Country)){
					$this->Country = $Country;
			}
        }

        public function getEmail() {
            return $this->Email;
        }

        public function setEmail($Email, $post=true) {
            			
			if($post && isset($_POST['Email'])){
				 
					$this->Email = ($_POST['Email']);
				 }
			if(!is_null($Email)){
					$this->Email = $Email;
			}
        }

        public function getFax() {
            return $this->Fax;
        }

        public function setFax($Fax, $post=true) {
          			
			if($post && isset($_POST['Fax'])){
				 
					$this->Fax = ($_POST['Fax']);
				 }
			if(!is_null($Fax)){
					$this->Fax = $Fax;
			}
        }

        public function getWebsite() {
            return $this->Website;
        }

        public function setWebsite($Website, $post=true) {
            			
			if($post && isset($_POST['Website'])){
				 
					$this->Website = ($_POST['Website']);
				 }
			if(!is_null($Website)){
					$this->Website = $Website;
			}
        }

        public function getSkypeId() {
            return $this->SkypeId;
        }

        public function setSkypeId($SkypeId, $post=true) {
           			
			if($post && isset($_POST['SkypeId'])){
				 
					$this->SkypeId = ($_POST['SkypeId']);
				 }
			if(!is_null($SkypeId)){
					$this->SkypeId = $SkypeId;
			}
        }

        public function getFbName() {
            return $this->FbName;
        }

        public function setFbName($FbName, $post=true) {
            			
			if($post && isset($_POST['FbName'])){
				 
					$this->FbName = ($_POST['FbName']);
				 }
			if(!is_null($FbName)){
					$this->FbName = $FbName;
			}
        }

        public function getTwitterName() {
            return $this->TwitterName;
        }

        public function setTwitterName($TwitterName, $post=true) {
            			
			if($post && isset($_POST['TwitterName'])){
				 
					$this->TwitterName = ($_POST['TwitterName']);
				 }
			if(!is_null($TwitterName)){
					$this->TwitterName = $TwitterName;
			}
        }

        public function getGplus() {
            return $this->Gplus;
        }

        public function setGplus($Gplus, $post=true) {
            			
			if($post && isset($_POST['Gplus'])){
				 
					$this->Gplus = ($_POST['Gplus']);
				 }
			if(!is_null($Gplus)){
					$this->Gplus = $Gplus;
			}
        }

        public function getYoutube() {
            return $this->Youtube;
        }

        public function setYoutube($Youtube, $post=true) {
            			
			if($post && isset($_POST['Youtube'])){
				 
					$this->Youtube = ($_POST['Youtube']);
				 }
			if(!is_null($Youtube)){
					$this->Youtube = $Youtube;
			}
        }


	}


?>
