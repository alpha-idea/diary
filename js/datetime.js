// Create two variable with the names of the months and days in an array
var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

var hours;
var minutes;
var seconds;


$(document).ready(function() {



// Create a newDate() object
var newDate = new Date();
// Extract the current date from Date object
setEntryDate(newDate);
/*
newDate.setDate(newDate.getDate());
// Output the day, date, month and year   
$('#Date').html(dayNames[newDate.getDay()] + " " + ordinal_suffix(newDate.getDate()) + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());
*/
$('#Day').html(newDate.getDate());


setInterval( function() {
	// Create a newDate() object and extract the seconds of the current time on the visitor's
	 seconds = new Date().getSeconds();
	// Add a leading zero to seconds value
	$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
	},1000);
	
setInterval( function() {
	// Create a newDate() object and extract the minutes of the current time on the visitor's
	minutes = new Date().getMinutes();
	// Add a leading zero to the minutes value
	$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
    },1000);
	
setInterval( function() {
	// Create a newDate() object and extract the hours of the current time on the visitor's
	hours = new Date().getHours();
	// Add a leading zero to the hours value
	$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
    }, 1000);	
});


function setEntryDate(newDate){
	
	// Extract the current date from Date object
newDate.setDate(newDate.getDate());
// Output the day, date, month and year   
$('#Date').html(dayNames[newDate.getDay()] + " " + ordinal_suffix(newDate.getDate()) + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());
	
	
	}


function ordinal_suffix(i){
	
		var rem = i % 10;
		
		if(rem==1 && rem!=12){
				return i+"<sup>st</sup>";
			}
		if(rem==2 && rem!=12){
				return i+"<sup>nd</sup>";
			}
		if(rem==3 && rem!=13){
				return i+"<sup>rd</sup>";
			}
		return i+"<sup>th</sup>";
	}
