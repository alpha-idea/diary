var daySelected=null;
var week = new Array("Sunday","Monday","Tuesday","Wednesday", "Thursday","Friday","Saturday");
var month = new Array("January","February","March","April", "May","June","July","August","September","October","November","December");
var weekIndex=null;
var monthIndex=null;
var en_day;
var en_month;
function viewDayPopup(dayObj){
	//console.log(dayObj);
	weekIndex = dayObj.date._d.getDay();
	monthIndex = dayObj.date._d.getMonth();
	en_day = week[weekIndex];
	en_month = month[monthIndex];
	
daySelected = dayObj.date._i;
console.log(daySelected);
$('.taskModal').find('#item-1').load('popup/day-popup.php',{clicked_date:daySelected,en_month:en_month,en_day:en_day},function(){});
$('.taskModal').show();

}

function showAddViewTasks(dayObj){
		
		$('#item-1').load('html/viewTask.html');
		
		$('#item-2').load('html/taskForm.html');
	
	}
	
function addTask(){
		
		var TaskTitle = $('[name=TaskTitle]').val();
		var TaskDescription = $('[name=TaskDescription]').val();
		var TaskAP = $('[name=TaskAP]').val();
		var TaskHour = $('[name=TaskHour]').val();
		var TaskMin = $('[name=TaskMin]').val();
		var TaskTime ;
		var AlarmSettings = $('[name=AlarmSettings]').val();
		
		var AlarmAP = $('[name=AlarmAP]').val();
		var AlarmHour = $('[name=AlarmHour]').val();
		var AlarmMin = $('[name=AlarmMin]').val();
		var AlarmTime ;
		var AlarmDate = $("#alarm-date-picker").val();
		switch(AlarmSettings){
			case '0':
				//Alarm date and time = task date and time
			break;
			
			case '1':
				//Alarm to custom time
						if(AlarmAP=="p" && parseInt(AlarmHour) < 12){
					
							AlarmHour = parseInt(AlarmHour)+12;
					
						}
						if(AlarmAP=="a" && parseInt(AlarmHour) == 12){
					
						AlarmHour = 00;
					
						}
		
						AlarmTime = AlarmHour+":"+AlarmMin+":00";
			break;
			
			case '2':
				//No Alarm
						
			break;
			
			default:
				//code here
			
			}
		
		
		if(TaskAP=="p" && parseInt(TaskHour) < 12){
					
					TaskHour = parseInt(TaskHour)+12;
					
			}
		if(TaskAP=="a" && parseInt(TaskHour) == 12){
					
					TaskHour = 00;
					
			}
		
		TaskTime = TaskHour+":"+TaskMin+":00";
		
		$.post('TaskHandler.php', {task_add:1,
								TaskTitle:TaskTitle,
								TaskDescription:TaskDescription,
								TaskTime:TaskTime,
								TaskDate:daySelected,
								AlarmSettings:AlarmSettings,
								AlarmTime:AlarmTime,
								AlarmDate:AlarmDate,
								}, 
				function(data,status){
					
					console.log(status);
					//alert(data);
					$('#add-task-status').show();
					$('[name=TaskTitle]').val("");
					$('[name=TaskDescription]').val("");
					$('[name=TaskHour]').val("1");
					$('[name=TaskMin]').val("00");
					$('[name=AlarmSettings]').val("0");
					$('[id=alarm-date-picker]').val("");
					
					$('#add-task-status').hide(4000);
			} );
		
	}

function deleteTask(taskid){
		
		$.ajax({
			type:"POST",
			url:"TaskHandler.php",
			async:false,
			dataType:'json',
			data:{task_delete:1, task_id:taskid}
		
		}).done(function (data){
			
				console.log(data);
				
				$("[data-id="+taskid+"]").parent().parent().remove();
			});
	
	
	}
	
	
function editTask(taskid){
	
	
	
	}

function autoSaveDiary(){
	
		$.ajax({
						
						url:'DiaryHandler.php',
						type:'POST',
						data:{diary_auto_save:1, EntryDate:daySelected, EntryText:$('#Diary').html()},
						success:function(){}
					
					
					
					});
			
	
	}

function loadDiaryContent(){
	
						$.ajax({
						
						url:'DiaryHandler.php',
						type:'POST',
						async:false,
						data:{diary_get_by_date:1, entry_date:daySelected},
						success:function(data){
							//alert("success");
								
										$("#Diary").html(data);
									//cke.insertHtml(data,"unfiltered_html");
								
							}
					
					
					
					});
	
	
	}
