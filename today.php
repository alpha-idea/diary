<?php
include_once("SessionHandler.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Today</title>

<link rel="stylesheet" href="style/today.css">

<link href='http://fonts.googleapis.com/css?family=Bonbon' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans|Comfortaa:300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400' rel='stylesheet' type='text/css'>
<link href="style/perfect-scrollbar.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="js/jquery-1.9.1.js"></script>

<script src="js/json2.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="js/moment-2.2.1.js"></script>



<script src="js/dayPopup.js"></script>

<script src="ckeditor/ckeditor.js"></script>
<script src="js/datetime.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/perfect-scrollbar.js"></script>
<script type="text/javascript">
var calendars = {};
var eventArray ;
  $(document).ready(function ($) {
	$('#diaryEntry').perfectScrollbar({
	  wheelSpeed: 20,
	  wheelPropagation: false
	});
	
/*
$.ajax({url:"events.php",
		dataType:"json",
		//cache:false,
		async:false,
		data:{yyyy_mm:moment().format('YYYY-MM')}, 
		success:function(data,status){
			console.log(data);
			eventArray = data;
			
			}
	});
*/	
	console.log(eventArray);
	 calendars.clndr1 = $('.cal2').clndr({
    //events: eventArray,
    // constraints: {
    //   startDate: '2013-11-01',
    //   endDate: '2013-11-15'
    // },
    clickEvents: {
      click: function(target) {
        //console.log(target);
      viewDayPopup(target);
        
        // if you turn the `constraints` option on, try this out:
        // if($(target.element).hasClass('inactive')) {
        //   console.log('not a valid datepicker date.');
        // } else {
        //   console.log('VALID datepicker date.');
        // }
      },
      nextMonth: function() {
        console.log('next month.');
      },
      previousMonth: function() {
        
      },
      onMonthChange: function() {
    /*    
        
        //console.log(this.month.format('YYYY-MM'));
		$.ajax({url:"events.php",
		dataType:"json",
		//cache:false,
		async:false,
		data:{yyyy_mm:this.month.format('YYYY-MM')}, 
		success:function(data,status){
			console.log(data);
			eventArray = data;
			
			}
	});
     console.log(eventArray);
      this.setEvents(eventArray);
     */   
        
      },
      nextYear: function() {
        console.log('next year.');
      },
      previousYear: function() {
        console.log('previous year.');
      },
      onYearChange: function() {
        console.log('year changed.');
      }
    },
    multiDayEvents: {
      startDate: 'startDate',
      endDate: 'endDate'
    },
    showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
  });
		
  });
</script>

<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="style/modal.css" type="text/css">
</head>

<body>
  <div id="diary">
    <div id="left_page">
      <div id="left_con">
        <div id="dateCon">
          <div id="dateNum">
           <div id="dateNumCon"><div id="Day"></div></div>
           <div id="clock"><span id="hours"> </span><span id="point">:</span><span id="min"> </span><span id="point">:</span><span id="sec"> </span></div>
          </div>
          <div id="fullDate"><div id="Date"></div></div>
        </div>
        <!--<div id="calendarCon">
        <script>
	var cal ;
$(document).ready(function(){	
var date = new Date();

cal = {year:date.getFullYear(), month:(date.getMonth()+1), day:date.getDate()};	

getCAL();
});

function getCAL()
    {
		

		if(cal.month == 13){
			cal.month = 1;
			cal.year++;
			
		}
		if(cal.month == 0){
			cal.month = 12;
			cal.year--;	
		}
		
		$("[id=month]").val(cal.month);
		$("[id=year]").val(cal.year);
		
		//$("#calendar").html(id).show();
			 $.get("pparses/calendar.php",{ month: cal.month, year:cal.year, day:cal.day } , function(result) {
				$("#calendar").html(result);
			 });
    };
	


</script>
         <div id="calendar">
		 </div>
        </div>-->
        <div class="cal2">
    	 </div>
        <!--<div id="searchEntry">
            <input type="search" >
          </div>-->
        <div id="diaryCon">
          
          <div id="diaryEntry">
           <div class="page" contenteditable="true">
            <p>Dear Diary,</p>
           </div>
          </div>
        </div>
      </div>
      <div id="right_con">
      </div>
    </div>
  </div>

<div class="modal-overlay">
		
		<div id="modal" class="modal">
			
			<a href='#' onclick="event.preventDefault();$('.modal-overlay').hide();">
			
			<img src='images/button_cancel.png' class='close-box'/>
			</a>
				<div id="item-1"></div>
				<div id="item-2"></div>
				<div id="item-3"></div>
				
		</div>
  </div>
<div id="adding-events">


</div>

<script src="js/clndr.js"></script>
<link rel="stylesheet" href="style/clndr.css">


</body>
</html>
