SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

<<<<<<< HEAD
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
=======
CREATE SCHEMA IF NOT EXISTS `diary` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `diary` ;

-- -----------------------------------------------------
-- Table `diary`.`Users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`Users` (
  `userid` INT NOT NULL AUTO_INCREMENT,
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
  `firstname` VARCHAR(45) NULL,
  `middlename` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `email` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  `gender` ENUM('m','f') NULL DEFAULT 'm',
  `birthday` VARCHAR(45) NULL,
  `profilepic` VARCHAR(255) NULL,
  `datecreated` DATETIME NULL,
  `lastlogin` DATETIME NULL,
  `lastactive` DATETIME NULL,
  `mobilePhone` VARCHAR(45) NULL,
  `businessPhone` VARCHAR(45) NULL,
  `homePhone` VARCHAR(45) NULL,
  `faxNumber` VARCHAR(45) NULL,
  `drivingLicence` VARCHAR(45) NULL,
  `passportNum` VARCHAR(45) NULL,
  `bloodgrp` VARCHAR(5) NULL,
<<<<<<< HEAD
  PRIMARY KEY (`id`))
=======
  PRIMARY KEY (`userid`))
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`entries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`entries` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `entry` TEXT NULL,
  `entrydate` VARCHAR(45) NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`, `User_id`),
  INDEX `fk_entries_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_entries_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `mydb`.`User` (`id`)
=======
-- Table `diary`.`entries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`entries` (
  `entryid` INT NOT NULL AUTO_INCREMENT,
  `entry` TEXT NULL,
  `entrydate` VARCHAR(45) NULL,
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`entryid`, `User_userid`),
  INDEX `fk_entries_User1_idx` (`User_userid` ASC),
  CONSTRAINT `fk_entries_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`tasks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `task` TEXT NULL,
  `taskdate` VARCHAR(45) NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`, `User_id`),
  INDEX `fk_tasks_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_tasks_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `mydb`.`User` (`id`)
=======
-- Table `diary`.`tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`tasks` (
  `taskid` INT NOT NULL AUTO_INCREMENT,
  `task` TEXT NULL,
  `taskdate` VARCHAR(45) NULL,
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`taskid`, `User_userid`),
  INDEX `fk_tasks_User1_idx` (`User_userid` ASC),
  CONSTRAINT `fk_tasks_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`alarm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`alarm` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_time_created` DATETIME NULL,
  `alarm_date` DATE NULL,
  `alarm_time` TIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`alarm_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`alarm_types` (
  `id` INT NOT NULL,
  `alarm_type` VARCHAR(45) NULL,
  `alarm_id` INT NOT NULL,
  PRIMARY KEY (`id`, `alarm_id`),
  INDEX `fk_alarm_types_alarm1_idx` (`alarm_id` ASC),
  CONSTRAINT `fk_alarm_types_alarm1`
    FOREIGN KEY (`alarm_id`)
    REFERENCES `mydb`.`alarm` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'alarm types: birthdays, anniversries, memo, reminders, tasks';


-- -----------------------------------------------------
-- Table `mydb`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`contacts` (
  `id` INT NOT NULL,
  PRIMARY KEY (`id`))
=======
-- Table `diary`.`alarm_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm_types` (
  `alarm_id` INT NOT NULL,
  `alarm_type` VARCHAR(45) NULL,
  PRIMARY KEY (`alarm_id`))
ENGINE = InnoDB
COMMENT = 'alarm types: birthdays, anniversries, memo, reminders, tasks';


-- -----------------------------------------------------
-- Table `diary`.`alarm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`alarm` (
  `idalarm` INT NOT NULL AUTO_INCREMENT,
  `alarm_types_alarm_id` INT NOT NULL,
  `date_time_created` DATETIME NULL,
  `alarm_date` DATE NULL,
  `alarm_time` TIME NULL,
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`idalarm`, `alarm_types_alarm_id`, `User_userid`),
  INDEX `fk_alarm_alarm_types1_idx` (`alarm_types_alarm_id` ASC),
  INDEX `fk_alarm_User1_idx` (`User_userid` ASC),
  CONSTRAINT `fk_alarm_alarm_types1`
    FOREIGN KEY (`alarm_types_alarm_id`)
    REFERENCES `diary`.`alarm_types` (`alarm_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_alarm_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`contacts` (
  `idcontacts` INT NOT NULL,
  PRIMARY KEY (`idcontacts`))
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`calender_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`calender_type` (
  `User_id` INT NOT NULL,
  PRIMARY KEY (`User_id`),
  CONSTRAINT `fk_calender_type_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `mydb`.`User` (`id`)
=======
-- Table `diary`.`calender_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`calender_type` (
  `User_userid` INT NOT NULL,
  PRIMARY KEY (`User_userid`),
  CONSTRAINT `fk_calender_type_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`anniversary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`anniversary` (
  `id` INT NOT NULL,
  `contacts_id` INT NOT NULL,
  PRIMARY KEY (`id`, `contacts_id`),
  INDEX `fk_anniversary_contacts1_idx` (`contacts_id` ASC),
  CONSTRAINT `fk_anniversary_contacts1`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `mydb`.`contacts` (`id`)
=======
-- Table `diary`.`anniversary`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`anniversary` (
  `anniversary_id` INT NOT NULL,
  `contacts_idcontacts` INT NOT NULL,
  PRIMARY KEY (`anniversary_id`, `contacts_idcontacts`),
  INDEX `fk_anniversary_contacts1_idx` (`contacts_idcontacts` ASC),
  CONSTRAINT `fk_anniversary_contacts1`
    FOREIGN KEY (`contacts_idcontacts`)
    REFERENCES `diary`.`contacts` (`idcontacts`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`birthday`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`birthday` (
  `id` INT NOT NULL,
  `contacts_id` INT NOT NULL,
  PRIMARY KEY (`id`, `contacts_id`),
  INDEX `fk_birthday_contacts1_idx` (`contacts_id` ASC),
  CONSTRAINT `fk_birthday_contacts1`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `mydb`.`contacts` (`id`)
=======
-- Table `diary`.`birthday`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`birthday` (
  `birthday_id` INT NOT NULL AUTO_INCREMENT,
  `contacts_idcontacts` INT NOT NULL,
  PRIMARY KEY (`birthday_id`, `contacts_idcontacts`),
  INDEX `fk_birthday_contacts1_idx` (`contacts_idcontacts` ASC),
  CONSTRAINT `fk_birthday_contacts1`
    FOREIGN KEY (`contacts_idcontacts`)
    REFERENCES `diary`.`contacts` (`idcontacts`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`user_contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user_contacts` (
  `id` INT NOT NULL,
  `contacts_id` INT NOT NULL,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`, `contacts_id`, `User_id`),
  INDEX `fk_user_contacts_contacts1_idx` (`contacts_id` ASC),
  INDEX `fk_user_contacts_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_user_contacts_contacts1`
    FOREIGN KEY (`contacts_id`)
    REFERENCES `mydb`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contacts_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `mydb`.`User` (`id`)
=======
-- Table `diary`.`user_contacts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`user_contacts` (
  `id_user_contacts` INT NOT NULL,
  `User_userid` INT NOT NULL,
  `contacts_idcontacts` INT NOT NULL,
  PRIMARY KEY (`id_user_contacts`, `User_userid`, `contacts_idcontacts`),
  INDEX `fk_user_contacts_User1_idx` (`User_userid` ASC),
  INDEX `fk_user_contacts_contacts1_idx` (`contacts_idcontacts` ASC),
  CONSTRAINT `fk_user_contacts_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_contacts_contacts1`
    FOREIGN KEY (`contacts_idcontacts`)
    REFERENCES `diary`.`contacts` (`idcontacts`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
<<<<<<< HEAD
-- Table `mydb`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`email` (
  `email` VARCHAR(255) NOT NULL,
  `verifyCode` VARCHAR(255) NULL,
  `verified` TINYINT(1) NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `User_id` INT NOT NULL,
  PRIMARY KEY (`id`, `User_id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC),
  INDEX `fk_email_User1_idx` (`User_id` ASC),
  CONSTRAINT `fk_email_User1`
    FOREIGN KEY (`User_id`)
    REFERENCES `mydb`.`User` (`id`)
=======
-- Table `diary`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`email` (
  `email` VARCHAR(255) NOT NULL,
  `verifyCode` VARCHAR(255) NULL,
  `verified` TINYINT(1) NULL,
  `User_userid` INT NOT NULL,
  `emailType` TINYINT(1) NULL,
  PRIMARY KEY (`email`, `User_userid`),
  INDEX `fk_email_User1_idx` (`User_userid` ASC),
  CONSTRAINT `fk_email_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `diary`.`Users` (`userid`)
>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

<<<<<<< HEAD
=======
USE `diary` ;

-- -----------------------------------------------------
-- Placeholder table for view `diary`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`view1` (`id` INT);

-- -----------------------------------------------------
-- View `diary`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `diary`.`view1`;
USE `diary`;

>>>>>>> 7ba31b9c315f357bbeb5875dc2eee17d0dec90d9

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
