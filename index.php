<?php
//include_once("SessionHandler.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Diary</title>
<link rel="stylesheet" href="style/imaindex.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="js/datetime.js"></script>

</head>

<body>
 <div id="diaryCon">
  <div id="diary">
   <div id="leftPage">
    <div id="leftHead">
     <div id="dateNumCon"><div id="Day"></div></div>
    </div>
   </div>
   <div id="rightPage">
    <div id="rightHead">
     <div id="clock"><span id="hours"> </span><span id="point">:</span><span id="min"> </span><span id="point">:</span><span id="sec"> </span></div>
     <div id="fullDate"><div id="Date"></div></div>
    </div>
    <div id="ridghtRul">
     <div id="suformCon">
     <script type="text/javascript">
	 var RecaptchaOptions = {
		theme : 'clean'
	 };
	 $(document).ready(function(){
			
				
		 
		 });
	 </script>
	 <div id="signup-box">
		 <p>Already a member? <a href="#" onclick="event.preventDefault();$('#signup-box').hide();$('#login-box').show(function(){$(this).find('table').effect('highlight',{color:'#B2D5EE'},1500);});">Login</a></p>
     <form action="verify.php" method="post">
      <table border="0">
        <tr>
          <td>First Name</td>
          <td><input type="text" id="fname" name="FirstName" placeholder="First Name"></td>
        </tr>
        <tr>
          <td>Last Name</td>
          <td><input type="text" id="lname" name="LastName" placeholder="Last Name"></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><input type="email" id="suemail" name="Email" placeholder="Email"></td>
        </tr>
        <tr>
          <td>Confirm Email</td>
          <td><input type="email" id="suconemail" name="suconemail" placeholder="Confirm Email"></td>
        </tr>
        <tr>
          <td>Password</td>
          <td><input type="password" id="supwd" name="Password" placeholder="Password"></td>
        </tr>
        <tr>
          <td>Confirm Password</td>
          <td><input type="password" id="suconpwd" name="suconpwd" placeholder="Confirm Password"></td>
        </tr>
        <tr>
          <td colspan="2">
            <div id="captchaCon">
            <?php
            require_once('pparses/recaptchalib.php');
            $publickey = "6LdbrewSAAAAAIXN-uBiZeGLv_ZZ8mFMiKCMsMky"; // you got this from the signup page
            echo recaptcha_get_html($publickey);
            ?>
            </div>
          </td>
        </tr>
        <tr>
          <td colspan="2"><input type="submit" id="submit" name="Signup" value="Sign Up" class="btn"></td>
        </tr>

      </table>

     </form>
     </div><!-- END SIGN UP FORM -->
     
    <div id="login-box" style="display:none;">
	<p>Not a member? <a href="#" onclick="event.preventDefault();$('#signup-box').show(function(){$(this).find('table').effect('highlight',{color:'#B2D5EE'},1500);});$('#login-box').hide();">Sign Up</a></p>
	
     <form action="login.php" method="post">
      <table border="0">
        
        <tr>
          <td>Email</td>
          <td><input type="email" id="suemail" name="Email" placeholder="Email"></td>
        </tr>
        
        <tr>
          <td>Password</td>
          <td><input type="password" id="supwd" name="Password" placeholder="Password"></td>
        </tr>
        
        <tr>
          <td colspan="2"><input type="submit" id="submit" name="Login" value="Login" class="btn"></td>
        </tr>

      </table>

     </form>
     </div><!-- END SIGN UP FORM -->
     
     </div>
    </div>
   </div>
  </div>
 </div>
</body>
</html>
